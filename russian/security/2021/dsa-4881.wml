#use wml::debian::translation-check translation="1ee140ecea4124e2849e2bc5c2de4b63b838e5c5" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В cURL, библиотека передачи URL, были обнаружены многочисленные уязвимости:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8169">CVE-2020-8169</a>

    <p>Марек Шлягор сообщил, что libcurl может добавлять часть пароля
    к имени узла до разрешения имени, что потенциально приводит к
    утечке части пароля по сети и DNS-серверам.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8177">CVE-2020-8177</a>

    <p>sn сообщил, что curl из-за вредоносного сервера может перезаписать
    локальный файл, если в команде одновременно используются опции -J (--remote-header-name)
    и -i (--include).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8231">CVE-2020-8231</a>

    <p>Марк Алдораси сообщил, что libcurl может использовать неверное соединение,
    когда приложение, использующее множественные API libcurl, устанавливает
    опцию CURLOPT_CONNECT_ONLY, что приводит к утечкам информации.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

    <p>Варнавас Папаиоанноу сообщил, что вредоносный сервер может использовать
    PASV-ответ для того, чтобы заставить curl подключиться к произвольному IP-адресу
    и порту, что может приводить к тому, что curl раскрывает информацию о
    службах, которые в обратном случае являются частными и не раскрываются.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

    <p>xnynx сообщил, что у libcurl может закончится место для стека, когда используется
    функционал сопоставления по шаблону FTP (CURLOPT_CHUNK_BGN_FUNCTION).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

    <p>Было сообщено, что libcurl не проверяет, что OCSP-ответ
    действительно совпадает с сертификатом, с которым он должен совпадать.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22876">CVE-2021-22876</a>

    <p>Виктор Шакац сообщил, что libcurl не удаляет данные учётной записи пользователя
    из URL, когда автоматически заполняет заголовок HTTP-запроса Referer
    в исходящих HTTP-запросах.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22890">CVE-2021-22890</a>

    <p>Минтао Ян сообщил, что при использовании HTTPS-прокси и TLS 1.3
    libcurl может спутать билеты сессий, поступающие от HTTPS-прокси, с билетами,
    полученными от удалённого сервера. Это может позволить HTTPS-прокси
    заставить libcurl использовать неправильный билет сессии для этого узла и
    тем самым обойти проверку TLS-сертификата сервера.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 7.64.0-4+deb10u2.</p>

<p>Рекомендуется обновить пакеты curl.</p>

<p>С подробным статусом поддержки безопасности curl можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4881.data"
