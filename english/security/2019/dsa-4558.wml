<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the webkit2gtk web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8625">CVE-2019-8625</a>

    <p>Sergei Glazunov discovered that maliciously crafted web content
    may lead to universal cross site scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8720">CVE-2019-8720</a>

    <p>Wen Xu discovered that maliciously crafted web content may lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8769">CVE-2019-8769</a>

    <p>Pierre Reimertz discovered that visiting a maliciously crafted
    website may reveal browsing history.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8771">CVE-2019-8771</a>

    <p>Eliya Stein discovered that maliciously crafted web content may
    violate iframe sandboxing policy.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.26.1-3~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4558.data"
# $Id: $
