<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in Thunderbird, which may lead
to the execution of arbitrary code or denial of service.</p>

<p>Debian follows the Thunderbird upstream releases. Support for the 68.x
series has ended, so starting with this update we're now following
the 78.x releases.</p>

<p>The 78.x series discontinues support for some addons. Also, starting
with 78, Thunderbird supports OpenPGP natively. If you are currently
using the Enigmail addon for PGP, please refer to the included NEWS
and README.Debian.gz files for information on how to migrate your
keys.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1:78.3.1-2~deb10u2.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>For the detailed security status of thunderbird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/thunderbird">\
https://security-tracker.debian.org/tracker/thunderbird</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4770.data"
# $Id: $
