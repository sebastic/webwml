<define-tag pagetitle>Debian Installer Bullseye RC 1 release</define-tag>
<define-tag release_date>2021-04-23</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the first release candidate of the installer for Debian 11
<q>Bullseye</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>base-installer:
    <ul>
      <li>Re-add the removed is_ports_architecture() function - it's used by
	other packages and library.sh looks like a fair central place for it
	to be (<a href="https://bugs.debian.org/979193">#979193</a>).</li>
    </ul>
  </li>
  <li>brltty:
    <ul>
      <li>Update udev rules.</li>
      <li>Make sure the hardening flags are used in the udeb build.</li>
    </ul>
  </li>
  <li>choose-mirror:
    <ul>
      <li>Update Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>debian-archive-keyring:
    <ul>
      <li>Add bullseye keys, remove jessie keys.</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Shorten "ppc64el" to "p64el" in the volume ID.</li>
      <li>Include eatmydata deb for the installer to use offline (<a href="https://bugs.debian.org/986772">#986772</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Build in libinput instead of evdev driver for the graphical
	installer. This should improve support for touchpads in particular.</li>
      <li>Bump Linux kernel ABI to 5.10.0-6.</li>
      <li>Add wireless-regdb-udeb to all Linux arch images (<a href="https://bugs.debian.org/979104">#979104</a>).</li>
      <li>Correct keyword for bootloader arguments in netboot/xen/debian.cfg
	example file (<a href="https://bugs.debian.org/904131">#904131</a>).</li>
      <li>Update debian-internals manual to current development state.</li>
      <li>Update minimum disk and memory sizes.</li>
    </ul>
  </li>
  <li>debian-installer-utils:
    <ul>
      <li>list-devices-linux: Support partitions on USB UAS devices.</li>
    </ul>
  </li>
  <li>espeakup:
    <ul>
      <li>Carry over the alsa mixer levels into the installed system.</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Make sure that efivarfs is loaded and the efivars pseudofs is
	mounted when needed.</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Add support for SBAT.</li>
      <li>grub-install: Fix inverted test for NLS enabled when copying locales
	(<a href="https://bugs.debian.org/979754">#979754</a>).</li>
    </ul>
  </li>
  <li>installation-report:
    <ul>
      <li>bugscript: do not include template in script output (<a href="https://bugs.debian.org/980929">#980929</a>).</li>
    </ul>
  </li>
  <li>libdebian-installer:
    <ul>
      <li>Remove the arbitrary limitation on maximum line length in Packages
	and Sources files (<a href="https://bugs.debian.org/971946">#971946</a>).</li>
    </ul>
  </li>
  <li>libinih:
    <ul>
      <li>Add libinih1-udeb for Debian Installer (<a href="https://bugs.debian.org/981864">#981864</a>).</li>
    </ul>
  </li>
  <li>libinput:
    <ul>
      <li>Drop libwacom support from the udeb.</li>
    </ul>
  </li>
  <li>libmd:
    <ul>
      <li>Add a udeb package needed by libbsd.</li>
    </ul>
  </li>
  <li>libwacom:
    <ul>
      <li>Drop the udeb, libinput no longer needs it.</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>Add bonding driver to the nic-modules udeb.</li>
      <li>Remove efivars from the efi-modules udeb.</li>
      <li>arm64: Fix i2c-mv64xxx module name.</li>
    </ul>
  </li>
  <li>localechooser:
    <ul>
      <li>Disable Kabyle for the text-based installer, as switching keyboard
	fails there (<a href="https://bugs.debian.org/973455">#973455</a>).</li>
      <li>Correct Kabyle locale definition in languagelist to its real naming.</li>
    </ul>
  </li>
  <li>lowmem:
    <ul>
      <li>Delete graphical terminal related files when memory is low (<a href="https://bugs.debian.org/977490">#977490</a>).</li>
      <li>5lowmem: Rename to 05lowmem so that rescue actually overrides it
	(<a href="https://bugs.debian.org/870574">#870574</a>).</li>
      <li>S15lowmem: Update minimum memory sizes.</li>
    </ul>
  </li>
  <li>nano:
    <ul>
      <li>Compile the udeb with --without-included-regex to make binaries
	substantially smaller.</li>
    </ul>
  </li>
  <li>netcfg:
    <ul>
      <li>Update testcases to work with current Check API (<a href="https://bugs.debian.org/980607">#980607</a>).</li>
      <li>Satisfy GCC pedantry for strncpy() calls.</li>
    </ul>
  </li>
  <li>os-prober:
    <ul>
      <li>Probe Microsoft OS on arm64.</li>
    </ul>
  </li>
  <li>partman-btrfs:
    <ul>
      <li>Add minimal subvolume support for / (<a href="https://bugs.debian.org/964818">#964818</a>).</li>
    </ul>
  </li>
  <li>partman-efi:
    <ul>
      <li>Make sure that efivarfs is loaded and the efivars pseudofs is
	mounted when needed.</li>
    </ul>
  </li>
  <li>rootskel:
    <ul>
      <li>Use /dev/tty0 as a console even if it's not in /proc/consoles.</li>
      <li>Do not remove the font when bterm is not started (<a href="https://bugs.debian.org/977466">#977466</a>).</li>
    </ul>
  </li>
  <li>rootskel-gtk:
    <ul>
      <li>Switch to Homeworld theme for Debian 11 by Juliette Taka.</li>
    </ul>
  </li>
  <li>user-setup:
    <ul>
      <li>Allow underscore in username of first account (<a href="https://bugs.debian.org/977214">#977214</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>arm64: Add support for puma-rk3399.</li>
      <li>arm64: Update to use u-boot-install-sunxi.</li>
      <li>armhf: Make hd-media and netboot sdcard images start at offset
	32768, for compatibility with rockchip platforms.</li>
      <li>arm64, armhf: Update SD-card-images sizes for netboot, netboot-gtk
	and hd-media.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add support for Orange Pi One Plus (<a href="https://bugs.debian.org/981328">#981328</a>).</li>
      <li>Add support for ROCK Pi 4 (A,B,C).</li>
      <li>Add support for Banana Pi BPI-M2-Ultra (<a href="https://bugs.debian.org/982089">#982089</a>).</li>
      <li>Add support for Banana Pi BPI-M3 (<a href="https://bugs.debian.org/981561">#981561</a>).</li>
      <li>Fix missing ESPRESSObin variants (<a href="https://bugs.debian.org/969518">#969518</a>). </li>
      <li>Fix several DTB-Id entries that incorrectly referenced the
	.dts instead of .dtb file.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
