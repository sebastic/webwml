<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in mpg123, an
MPEG layer 1/2/3 audio decoder and player. An attacker could take
advantage of these flaws to cause a denial of service against mpg123
or applications using the libmpg123 library with a carefully crafted
input file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9497">CVE-2014-9497</a>

    <p>Myautsai PAN discovered a flaw in the decoder initialization code
    of libmpg123. A specially crafted mp3 input file can be used to
    cause a buffer overflow, resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000247">CVE-2016-1000247</a>

    <p>Jerold Hoong discovered a flaw in the id3 tag processing code of
    libmpg123. A specially crafted mp3 input file could be used to
    cause a buffer over-read, resulting in a denial of service.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.14.4-1+deb7u1.</p>

<p>We recommend that you upgrade your mpg123 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-655.data"
# $Id: $
