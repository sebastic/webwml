<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An old attack vector has been corrected in bash, a sh-compatible
command language interpreter.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7543">CVE-2016-7543</a>

  <p>Specially crafted SHELLOPTS+PS4 environment variables in combination
  with insecure setuid binaries can result in root privilege
  escalation.</p>

<p>The setuid binary had to both use setuid() function call in
combination with a system() or popen() function call. With this
combination it is possible to gain root access.</p>

<p>I addition bash have to be the default shell (/bin/sh have to point
to bash) for the system to be vulnerable.</p>

<p>The default shell in Debian is dash and there are no known setuid
binaries in Debian with the, above described, insecure combination.</p>

<p>There could however be local software with the, above described,
insecure combination that could benefit from this correction.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this problem have been fixed in version
4.2+dfsg-0.1+deb7u3.</p>

<p>We recommend that you upgrade your bash packages.</p>

<p>If there are local software that have the insecure combination and
do a setuid() to some other user than root, then the update will not
correct that problem. That problem have to be addressed in the
insecure setuid binary.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-680.data"
# $Id: $
