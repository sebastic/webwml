<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in mosquitto, a MQTT version 3.1/3.1.1
compatible message broker.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7655">CVE-2017-7655</a>

      <p>A Null dereference vulnerability in the Mosquitto library could
      lead to crashes for those applications using the library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12550">CVE-2018-12550</a>

      <p>An ACL file with no statements was treated as having a default
      allow policy. The new behaviour of an empty ACL file is a default
      policy of access denied.
      (this is in compliance with all newer releases)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12551">CVE-2018-12551</a>

      <p>Malformed authentication data in the password file could allow
      clients to circumvent authentication and get access to the broker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11779">CVE-2019-11779</a>

      <p>Fix for processing a crafted SUBSCRIBE packet containing a topic
      that consists of approximately 65400 or more '/' characters.
      (setting TOPIC_HIERARCHY_LIMIT to 200)</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.4-2+deb8u4.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1972.data"
# $Id: $
