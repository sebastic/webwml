<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that pam-python, a PAM Module that runs the Python
interpreter, has an issue in regard to the default environment variable
handling of Python. This issue could allow for local root escalation in certain
PAM setups.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.4-1.1+deb8u1.</p>

<p>We recommend that you upgrade your pam-python packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2000.data"
# $Id: $
