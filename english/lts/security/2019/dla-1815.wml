<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the poppler PDF rendering
library, which could result in denial of service or possibly other
unspecified impact when processing malformed or maliciously crafted
files.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.26.5-2+deb8u10.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1815.data"
# $Id: $
