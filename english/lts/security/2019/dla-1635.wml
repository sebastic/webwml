<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was found in sssd. If a user was configured with no home
directory set, sssd would return '/' (the root directory) instead of ''
(the empty string / no home directory). This could impact services that
restrict the user's filesystem access to within their home directory
through chroot() etc.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.11.7-3+deb8u2.</p>

<p>We recommend that you upgrade your sssd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1635.data"
# $Id: $
