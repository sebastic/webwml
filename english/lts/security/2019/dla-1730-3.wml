<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various security problems have been additionally fixed in libssh2, an SSH
client implementation written in C.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>

    <p>While investigating the impact of <a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a> in Debian jessie's
    version of libssh2, it was discovered that issues around
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a> had not been fully resolved in Debian jessie's version
    of libssh2. A thorough manual (read, analyze, and copy code changes
    if needed) comparison of upstream code and code in Debian jessie's
    version of libssh2 was done and various more boundary checks and
    integer overflow protections got added to the package.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>

    <p>Kevin Backhouse from semmle.com discovered that initial fixes for the
    CVE series <a href="https://security-tracker.debian.org/tracker/CVE-2019-3855">CVE-2019-3855</a> - 2019-3863 introduced several regressions
    about signedness of length return values into the upstream code.
    While working on the <a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a> update mentioned above, it was
    paid attention to not introduce these upstream regression registered
    as <a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.4.3-4.1+deb8u4.</p>

<p>We recommend that you upgrade your libssh2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730-3.data"
# $Id: $
