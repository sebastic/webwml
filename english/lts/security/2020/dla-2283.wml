<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An HTTP request smuggling issue was discovered in the ngx_lua plugin
for nginx, a high-performance web and reverse proxy server, as
demonstrated by the ngx.location.capture API.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.10.3-1+deb9u5.</p>

<p>We recommend that you upgrade your nginx packages.</p>

<p>For the detailed security status of nginx please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nginx">https://security-tracker.debian.org/tracker/nginx</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2283.data"
# $Id: $
