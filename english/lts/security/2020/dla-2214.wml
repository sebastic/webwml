<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various vulnerabilities have been addressed in libexif, a library to
parse EXIF metadata files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6328">CVE-2016-6328</a>

    <p>An integer overflow when parsing the MNOTE entry data of the input
    file had been found. This could have caused Denial-of-Service (DoS)
    and Information Disclosure (disclosing some critical heap chunk
    metadata, even other applications' private data).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7544">CVE-2017-7544</a>

    <p>libexif had been vulnerable to out-of-bounds heap read vulnerability
    in exif_data_save_data_entry function in libexif/exif-data.c caused
    by improper length computation of the allocated data of an ExifMnote
    entry which could have caused denial-of-service or possibly information
    disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20030">CVE-2018-20030</a>

    <p>An error when processing the EXIF_IFD_INTEROPERABILITY and
    EXIF_IFD_EXIF tags within libexif version could have been exploited
    to exhaust available CPU resources.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0093">CVE-2020-0093</a>

    <p>In exif_data_save_data_entry of exif-data.c, there was a possible out
    of bounds read due to a missing bounds check. This could have lead to
    local information disclosure with no additional execution privileges
    needed. User interaction was needed for exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12767">CVE-2020-12767</a>

    <p>libexif had a divide-by-zero error in exif_entry_get_value in
    exif-entry.c</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.6.21-2+deb8u2.</p>

<p>We recommend that you upgrade your libexif packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2214.data"
# $Id: $
