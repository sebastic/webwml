<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an out-of-bounds write vulnerability
in pdfresurrect, a tool for extracting or scrubbing versioning data
from PDF documents.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9549">CVE-2020-9549</a>

    <p>In PDFResurrect 0.12 through 0.19, get_type in pdf.c has an
    out-of-bounds write via a crafted PDF document.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.12-5+deb8u1.</p>

<p>We recommend that you upgrade your pdfresurrect packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2134.data"
# $Id: $
