<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10208">CVE-2016-10208</a>

    <p>Sergej Schumilo and Ralf Spenneberg discovered that a crafted ext4
    filesystem could trigger memory corruption when it is mounted.  A
    user that can provide a device or filesystem image to be mounted
    could use this for denial of service (crash or data corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8824">CVE-2017-8824</a>

    <p>Mohamed Ghannam discovered that the DCCP implementation did not
    correctly manage resources when a socket is disconnected and
    reconnected, potentially leading to a use-after-free.  A local
    user could use this for denial of service (crash or data
    corruption) or possibly for privilege escalation.  On systems that
    do not already have the dccp module loaded, this can be mitigated
    by disabling it:
    echo &gt;&gt; /etc/modprobe.d/disable-dccp.conf install dccp false</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8831">CVE-2017-8831</a>

    <p>Pengfei Wang discovered that the saa7164 video capture driver
    re-reads data from a PCI device after validating it.  A physically
    present user able to attach a specially designed PCI device could
    use this for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12190">CVE-2017-12190</a>

    <p>Vitaly Mayatskikh discovered that the block layer did not
    correctly count page references for raw I/O from user-space.  This
    can be exploited by a guest VM with access to a host SCSI device
    for denial of service (memory exhaustion) or potentially for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a>

    <p>A vulnerability was found in the WPA2 protocol that could lead to
    reinstallation of the same Group Temporal Key (GTK), which
    substantially reduces the security of wifi encryption.  This is
    one of the issues collectively known as <q>KRACK</q>.</p>

    <p>Updates to GTKs are usually handled by the wpa package, where this
    issue was already fixed (DLA-1150-1).  However, some wifi devices
    can remain active and update GTKs autonomously while the system is
    suspended.  The kernel must also check for and ignore key
    reinstallation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14051">CVE-2017-14051</a>

    <p><q>shqking</q> reported that the qla2xxx SCSI host driver did not
    correctly validate I/O to the <q>optrom</q> sysfs attribute of the
    devices it creates.  This is unlikely to have any security
    impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15115">CVE-2017-15115</a>

    <p>Vladis Dronov reported that the SCTP implementation did not
    correctly handle "peel-off" of an association to another net
    namespace.  This leads to a use-after-free, which a local user can
    exploit for denial of service (crash or data corruption) or
    possibly for privilege escalation.  On systems that do not already
    have the sctp module loaded, this can be mitigated by disabling
    it:
    echo &gt;&gt; /etc/modprobe.d/disable-sctp.conf install sctp false</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15265">CVE-2017-15265</a>

    <p>Michael23 Yu reported a race condition in the ALSA sequencer
    subsystem involving creation and deletion of ports, which could
    lead to a use-after-free.  A local user with access to an ALSA
    sequencer device can use this for denial of service (crash or data
    loss) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15299">CVE-2017-15299</a>

    <p>Eric Biggers discovered that the KEYS subsystem did not correctly
    handle update of an uninstantiated key, leading to a null
    dereference.  A local user can use this for denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15649">CVE-2017-15649</a>

    <p><q>nixioaming</q> reported a race condition in the packet socket
    (AF_PACKET) implementation involving rebinding to a fanout group,
    which could lead to a use-after-free.  A local user with the
    CAP_NET_RAW capability can use this for denial of service (crash
    or data corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15868">CVE-2017-15868</a>

    <p>Al Viro found that the Bluebooth Network Encapsulation Protocol
    (BNEP) implementation did not validate the type of the second
    socket passed to the BNEPCONNADD ioctl(), which could lead to
    memory corruption.  A local user with the CAP_NET_ADMIN capability
    can use this for denial of service (crash or data corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16525">CVE-2017-16525</a>

    <p>Andrey Konovalov reported that the USB serial console
    implementation did not correctly handle disconnection of unusual
    serial devices, leading to a use-after-free.  A similar issue was
    found in the case where setup of a serial console fails.  A
    physically present user with a specially designed USB device can
    use this to cause a denial of service (crash or data corruption)
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16527">CVE-2017-16527</a>

    <p>Andrey Konovalov reported that the USB sound mixer driver did not
    correctly cancel I/O in case it failed to probe a device, which
    could lead to a use-after-free.  A physically present user with a
    specially designed USB device can use this to cause a denial of
    service (crash or data corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16529">CVE-2017-16529</a>

    <p>Andrey Konovalov reported that the USB sound driver did not fully
    validate descriptor lengths, which could lead to a buffer
    over-read.  A physically present user with a specially designed
    USB device may be able to use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16531">CVE-2017-16531</a>

    <p>Andrey Konovalov reported that the USB core did not validate IAD
    lengths, which could lead to a buffer over-read.  A physically
    present user with a specially designed USB device may be able to
    use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16532">CVE-2017-16532</a>

    <p>Andrey Konovalov reported that the USB test driver did not
    correctly handle devices with specific combinations of endpoints.
    A physically present user with a specially designed USB device can
    use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16533">CVE-2017-16533</a>

    <p>Andrey Konovalov reported that the USB HID driver did not fully
    validate descriptor lengths, which could lead to a buffer
    over-read.  A physically present user with a specially designed
    USB device may be able to use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16535">CVE-2017-16535</a>

    <p>Andrey Konovalov reported that the USB core did not validate BOS
    descriptor lengths, which could lead to a buffer over-read.  A
    physically present user with a specially designed USB device may
    be able to use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16536">CVE-2017-16536</a>

    <p>Andrey Konovalov reported that the cx231xx video capture driver
    did not fully validate the device endpoint configuration, which
    could lead to a null dereference.  A physically present user with
    a specially designed USB device can use this to cause a denial of
    service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16537">CVE-2017-16537</a>

    <p>Andrey Konovalov reported that the imon RC driver did not fully
    validate the device interface configuration, which could lead to a
    null dereference.  A physically present user with a specially
    designed USB device can use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16643">CVE-2017-16643</a>

    <p>Andrey Konovalov reported that the gtco tablet driver did not
    fully validate descriptor lengths, which could lead to a buffer
    over-read.  A physically present user with a specially designed
    USB device may be able to use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16649">CVE-2017-16649</a>

    <p>Bjørn Mork found that the cdc_ether network driver did not
    validate the device's maximum segment size, potentially leading to
    a division by zero.  A physically present user with a specially
    designed USB device can use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16939">CVE-2017-16939</a>

    <p>Mohamed Ghannam reported (through Beyond Security's SecuriTeam
    Secure Disclosure program) that the IPsec (xfrm) implementation
    did not correctly handle some failure cases when dumping policy
    information through netlink.  A local user with the CAP_NET_ADMIN
    capability can use this for denial of service (crash or data
    corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000407">CVE-2017-1000407</a>

    <p>Andrew Honig reported that the KVM implementation for Intel
    processors allowed direct access to host I/O port 0x80, which
    is not generally safe.  On some systems this allows a guest
    VM to cause a denial of service (crash) of the host.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.96-2.  This version also includes bug fixes from upstream versions
up to and including 3.2.96.  It also fixes some regressions caused by
the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>, 
which was included in DLA-993-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1200.data"
# $Id: $
