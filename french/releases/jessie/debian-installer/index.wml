#use wml::debian::template title="Debian Jessie : informations pour l'installation" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/jessie/release.data"
#use wml::debian::translation-check translation="c4da598be91aaeba098bf112270ab920ef3f6977" maintainer="Baptiste Jammet"

<h1>Installer Debian <current_release_jessie></h1>

<if-stable-release release="stretch">
<p><strong>La version 8 de Debian a été détrônée par
<a href="../../stretch/">la version 9 de Debian (<q>Stretch</q>)</a>.
Il est possible que certaines de ces images d'installation ne soient plus
disponibles ou ne fonctionnent plus correctement ; par conséquent,
il est fortement recommandé d'installer <q>Stretch</q>.
</strong></p>
</if-stable-release>

<p>
<strong>Pour installer Debian</strong> <current_release_jessie>
(<em>Jessie</em>), téléchargez l'une des images suivantes
(les images de CD et DVD pour les architectures i386 et amd64 peuvent être utilisées sur des clefs USB) :
</p>

<div class="line">
<div class="item col50">
	<p><strong>images de CD d'installation par le réseau (en général entre 150 et 280 Mo)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (utilisation de <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (utilisation de <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (utilisation de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (utilisation de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (utilisation de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>autres images (amorçage par le réseau, sur clef USB personnalisée, etc.)</strong></p>
<other-images />
</div>
</div>

<div class="warning">
<p>
Si un équipement du système <strong>nécessite le chargement d'un
microprogramme (« firmware »)</strong> avec le pilote approprié, vous
pouvez utiliser une des <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/jessie/current/">\
archives de paquets de microprogrammes courants</a> ou téléchargez une image
<strong>non officielle</strong> avec les microprogrammes intégrés.
Les instructions d'utilisation des archives et de chargement de microprogrammes pendant
l'installation sont disponibles dans le guide de l'installation (consultez la section
Documentation ci-dessous).
</p>
<div class="line">
<div class="item col50">
<p><strong>
images de CD <strong>non libres</strong> d'installation par le réseau
(en général entre 240 et 290 Mo) <strong>avec microprogrammes</strong>
</strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Notes</strong>
</p>
<ul>
    <li>
Pour télécharger les images complètes de CD et DVD, l'utilisation
de BitTorrent ou de jigdo est recommandée.
    </li><li> 
Pour les architectures les moins courantes, un nombre limité seulement d'images
des jeux de CD et DVD sont disponibles sous forme de fichiers ISO ou en utilisant
BitTorrent. Les jeux complets sont disponibles uniquement en utilisant jigdo.
    </li><li>
L'image <em>CD</em> multiarchitecture gère les architectures i386 et amd64 ;
l'installation est similaire à l'installation mono architecture depuis une image
de CD d'installation par le réseau.
    </li><li>
L'image <em>DVD</em> multiarchitecture gère les architectures
i386 et amd64 ; l'installation est similaire à l'installation mono
architecture depuis une image CD complète. Le DVD contient également les
sources des paquets.
    </li><li>
Pour les images d'installation, les fichiers de vérification
(<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> et autres)
sont disponibles dans le même dossier que les images.
    </li>
</ul>


<h1>Documentation</h1>

<p>
<strong>Si vous ne voulez lire qu'un document</strong> avant l'installation, lisez notre
<a href="../i386/apa">guide d'installation</a>, une marche à suivre rapide pour
le processus d'installation. Voici d'autres documents utiles :
</p>

<ul>
<li><a href="../installmanual">manuel d'installation de Jessie</a><br />
instructions détaillées pour l'installation ;</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ de l'installateur Debian</a>
et <a href="$(HOME)/CD/faq/">FAQ des CD Debian</a><br />
questions récurrentes et leurs réponses ;</li>
<li><a href="https://wiki.debian.org/DebianInstaller">wiki de l'installateur Debian</a><br />
documentation maintenue par la communauté.</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Voici une liste des problèmes connus dans l'installateur fourni avec Debian
<current_release_jessie>. Si vous avez rencontré un problème en
installant Debian et que vous ne le voyez pas listé ici, veuillez nous envoyer
un <a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">rapport
d'installation</a> (en anglais) décrivant le problème ou <a
href="https://wiki.debian.org/DebianInstaller/BrokenThings">vérifiez le
wiki</a> pour d'autres problèmes connus.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r11">Errata pour la publication 8.11</h3>
<dl class="gloss">

 <dt>pkgsel n'installe pas les mises à jour (par défaut) lors des changements d'ABI</dt>

 <dd>
 Bogue <a href="https://bugs.debian.org/908711">n° 908711</a> :
 lors d'une installation avec les sources réseau activées,
 les mises à jour de sécurité ne contiennent pas celles qui dépendent
 d'un nouveau paquet binaire qui contient un changement de l'ABI du
 noyau ou d'une bibliothèque.

 <br />
 <b>État :</b> ce problème a été corrigé dans l'installateur des
 nouvelles versions (à partir de Debian 9 Stretch). Pour les
 installations plus anciennes, il n'y a pas de nouvelle publication de
 l'installateur. Les mises à jour de sécurité qui dépendent de
 nouveaux paquets doivent être installées manuellement :
 <br /> – exécutez <code>apt-get upgrade --with-new-pkgs</code> ;
 <br /> – redémarrez pour compléter la mise à jour.
 </dd>

<dt>APT était vunérable à une attaque de type « homme du milieu »</dt>

<dd>
Un bogue dans la méthode de transport HTTP
(<a href="https://lists.debian.org/debian-lts-announce/2019/01/msg00014.html">CVE-2019-3462</a>)
pouvait être exploité par un attaquant dans la position « d'homme du milieu »
entre APT et un miroir pour forcer l'installation de paquets supplémentaires
malveillants.

<br />
Cela peut être contourné en désactivant le réseau pendant l'installation initiale,
puis en effectuant une mise à jour en suivant les recommandations de l'annonce
<a href="$(HOME)/lts/security/2019/dla-1637">DLA-1637</a>.

<br /> <b>État :</b> ce problème a été corrigé dans la version 8.11.1</dd>

</dl>

<h3 id="errata-r0">Errata pour la publication 8.0</h3>

<dl class="gloss">

<dt>L'installation d'un environnement de bureau pourrait échouer avec le seul premier CD</dt>

<dd>
En raison de sa taille,
la totalité du bureau GNOME ne rentre pas sur le premier CD.
Pour une installation de cet environnement, utilisez une source supplémentaire de paquets
(un second CD ou un miroir réseau) ou un DVD.
<br />
<b>État :</b> rien de plus ne peut être fait pour faire rentrer
davantage de paquets sur le premier CD.
</dd>

<dt>Message de démarrage obsolète sur les PowerPC</dt>

<dd>
Bogue <a href="https://bugs.debian.org/783569">n° 783569</a> :
les CD pour l'architecture PowerPC indiquent la ligne de commande du noyau à
entrer au démarrage pour choisir l'environnement de bureau.
Cela est maintenant obsolète car la sélection du bureau s'effectue dans un menu tasksel.
<br />
<b>État :</b> ce problème a été corrigé dans la version 8.1.
</dd>

<dt>Problèmes lors de l'installation de plusieurs tâches d'environnement de bureau</dt>

<dd>Bogue <a href="https://bugs.debian.org/783571">n° 783571</a> :
il n'est pas possible d'installer à la fois GNOME et Xfce. Il existe un conflit
dans les dépendances qui fait échouer l'installation.
<br />
<b>État :</b> ce problème a été corrigé dans la version 8.1.
</dd>

</dl>

<if-stable-release release="jessie">
<p>
Des versions améliorées du système d'installation sont développées pour la
prochaine publication de Debian et peuvent être également utilisées pour
installer Jessie.
Pour plus de détails, consultez la
<a href="$(HOME)/devel/debian-installer/">page du projet de l'installateur de
Debian.</a>.
</p>
</if-stable-release>
