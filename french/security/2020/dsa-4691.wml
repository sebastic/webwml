#use wml::debian::translation-check translation="7258e8ed24b368bd5d4247393060727d8f8cf318" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans PDNS Recursor, un serveur
de résolution de noms ; une attaque par amplification de trafic
à l'encontre de serveurs de noms tiers faisant autorité (NXNSAttack) et une
validation insuffisante de réponses NXDOMAIN dépourvues d'enregistrement
SOA.</p>

<p>La version de pdns-recursor dans la distribution oldstable (Stretch)
n'est plus prise en charge. Si ces problèmes de sécurité affectent votre
configuration, vous devriez mettre à niveau vers la distribution stable
(Buster).</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.1.11-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pdns-recursor.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pdns-recursor,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pdns-recursor">\
https://security-tracker.debian.org/tracker/pdns-recursor</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4691.data"
# $Id: $
