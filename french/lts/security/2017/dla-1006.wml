#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de  déni de service ont été identifiées dans
libarchive lors de la manipulation d’archives contrefaites pour l'occasion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10209">CVE-2016-10209</a>

<p>Déréférencement de pointeur NULL et plantage d'application dans la fonction
archive_wstring_append_from_mbs().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10349">CVE-2016-10349</a>

<p>Lecture hors limites de tampon basé sur le tas et plantage d'application dans
la fonction archive_le32dec().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10350">CVE-2016-10350</a>

<p>Lecture hors limites de tampon basé sur le tas et plantage d'application dans
la fonction archive_read_format_cab_read_header().</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.0.4-3+wheezy6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libarchive.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1006.data"
# $Id: $
