#use wml::debian::translation-check translation="6348d9150e64715cf9d6afd4f37474ae2b4efbc8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Wordpress, un cadriciel
populaire de gestion de contenu.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17670">CVE-2019-17670</a>

<p>WordPress possède une vulnérabilité SSRF (Serveur Side Request Forgery)
due à ce que les chemins de Windows sont mal gérés lors d’une certaine
validation d’URL relatif.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4047">CVE-2020-4047</a>

<p>Des utilisateurs authentifiés avec les droits de téléversement (tels que les
auteurs) sont capables d’injecter du JavaScript dans certaines pages
jointes à un fichier de média d’une certaine façon. Cela peut conduire à une
exécution de script dans le contexte d’un utilisateur ayant plus de droits quand
le fichier est vu par eux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4048">CVE-2020-4048</a>

<p>Dû à un problème dans wp_validate_redirect() et un nettoyage d’URL, un lien
arbitraire externe peut être contrefait, conduisant à une redirection
ouverte ou inattendue lors d’un clic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4049">CVE-2020-4049</a>

<p>Lors du téléversement de thème, le nom du dossier de thème peut être
contrefait d’une façon qui pourrait conduire à une exécution de JavaScript dans
/wp-admin dans la page de thèmes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4050">CVE-2020-4050</a>

<p>Une mauvaise utilisation de « set-screen-option », la valeur de retour du
filtre permet aux champs meta d’utilisateur arbitraire d’être sauvegardés. Cela
nécessite l’installation d’un greffon par un administrateur qui utiliserait mal
le greffon. Une fois installé, il peut être exploité par des utilisateurs ayant
peu de droits.</p></li>

</ul>

<p>De plus, cette mise à jour assure que les derniers commentaires ne peuvent
être vus qu’à partir de messages publics, et corrige la procédure d’activation
d’utilisateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.7.18+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2371.data"
# $Id: $
