#use wml::debian::translation-check translation="f17a010324e3819ce34a37bfe302a0fa111a04ad" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité dans la validation de <q>Certificate List Exact
Assertion</q> a été découverte dans OpenLDAP, une implémentation libre de
<q>Lightweight Directory Access Protocol</q>. Un attaquant distant non
authentifié peut exploiter ce défaut pour provoquer un déni de service
(plantage du démon slapd) à l’aide de paquets contrefaits pour l’occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.4.44+dfsg-5+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openldap.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openldap, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openldap">\
https://security-tracker.debian.org/tracker/openldap</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2574.data"
# $Id: $
