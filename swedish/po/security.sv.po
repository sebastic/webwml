msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 18:13+0200\n"
"Last-Translator: Peter Karlsson <peterk@debian.org>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "Säkerhet i Debian"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "Säkerhetsbulletiner från Debian"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "F"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""
"<a href=\\\"undated/\\\">odaterade</a> säkerhetsbulletiner bevarade för "
"eftervärlden"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "Mitres CVE-förteckning"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "Securityfocus Bugtraqdatabas"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "CERT-bulletiner"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "US-CERTs sårbarheternotiser"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Källkod:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Arkitekturoberoende komponent:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"MD5-kontrollsummor för dessa filer finns i <a href=\"<get-var url />"
"\">originalbulletinen</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"MD5-kontrollsummor för dessa filer finns i <a href=\"<get-var url />"
"\">reviderade bulletinen</a>."

#: ../../english/template/debian/security.wml:44
msgid "Debian Security Advisory"
msgstr "Säkerhetsbulletin från Debian"

#: ../../english/template/debian/security.wml:49
msgid "Date Reported"
msgstr "Rapporterat den"

#: ../../english/template/debian/security.wml:52
msgid "Affected Packages"
msgstr "Berörda paket"

#: ../../english/template/debian/security.wml:74
msgid "Vulnerable"
msgstr "Sårbara"

#: ../../english/template/debian/security.wml:77
msgid "Security database references"
msgstr "Referenser i säkerhetsdatabaser"

#: ../../english/template/debian/security.wml:80
msgid "More information"
msgstr "Ytterligare information"

#: ../../english/template/debian/security.wml:86
msgid "Fixed in"
msgstr "Rättat i"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "BugTraq-id"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "Fel"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "I Debians felrapporteringssystem:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "I Bugtraq-databasen (hos SecurityFocus):"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "I Mitres CVE-förteckning:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "CERTs information om sårbarheter, bulletiner och incidenter:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr ""
"För närvarande är inga ytterligare referenser till externa "
"säkerhetsdatabaser tillgängliga."

#~ msgid "CERT alerts"
#~ msgstr "CERT-bulletiner"
