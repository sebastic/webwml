#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f"
<define-tag pagetitle>Ampere donerar Arm64-serverhårdvara till Debian för att förstärka Arm-ekosystemet</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
<a href="https://amperecomputing.com/">Ampere®</a> har samarbetat med Debian
för att stödja vår hårdvaruinfrastruktur genom donationen av
tre av Ampere's högpresterande Arm64-servrar.
Dessa Lenovo ThinkSystem HR330A-servrar innehåller Ampere's eMAG CPU med
en Arm®v8 64-bitars processor specfikt designad för molnservrar,
utrustad med 256 GB RAM, dubbla 960 GB SSDer och en 256 GbE dual port NIC.
</p>

<p>
De donerade servrarna har installerats på University of British Columbia,
vår värdpartner i Vancouver, Canada.
Debians Systemadministratörer (Debian System Administrators, DSA) har
konfigurerat dom att köra arm64/armhf/armel-byggdemoner, vilket ersätter
byggdemoner som kör på mindre kraftfulla utvecklingskort. På virtuella
maskiner med hälften så många allokerade VCPUer, har resultatet blivit att
tiden att bugger Arm*-paket har halverats med Ampere's eMAG-system. Ytterligare
en fördel tack vare denna generösa gåva är att det lp,,er att tillåta DSA
att migrera några allmäna Debiantjänster som för närvarande kör i vår
nuvarande infrastruktur, och kommer att tillhandahålla virtuella maskiner för
andra Debian-team (som Continuous Integration, kvalitetssäkeringsgruppen, osv.)
som behöver tillgång till Arm64-arkitekturen.
</p>

<p>
<q>Vårt partnerskap med Debian stöder vår utvecklarstrategi att expandera
öppenkällkodscommunityn som kör på Ampere-servrar för att ytterligare
bygga ut Arm64-ekosystemet och möjliggöra skapandet av nya applikationer.</q>,
säger Mauri Whalen, vicepresident för software engineering på Ampere.
<q>Debian är en välskött och respekterad community, och vi är stolta att
jobba med dem.</q>
</p>

<p>
<q>Debians Systemadministratörer är tacksamma till Ampere för donationen av
leverantörsklass Arm64-servrar. Att ha servrar med integrerade
standardhanteringsgränssnitt så som Intelligent Platform Management Interface
(IPMI), och med Lenovo's hårdvarugarantier och supportorganisation bakom sig
är precis vad DSA har önskat för Arm64-arkitekturen.
Dessa servrar är väldigt kraftfulla och välutrustade: vi räknar med att använda
dem för allmänna tjänster utöver Arm64-byggdemoner. Jag tror att dom kommer
att visa sig vara väldigt tilltalande för molnoperatörer och jag är stolt över
att Ampere Computing samarbetar med Debian.</q> - Luca Filipozzi, 
Debian Systemadminstratör.
</p>

<p>
Det är endast tack vare donationen av volontärinsatser, utrustning och
tjänster, samt finansiering som Debian har möjligheten att leverera på
vårt åtagande för ett fritt operativsystem. Vi är väldigt tacksamma för
Ampere's generositet.
</p>

<h2>Om Ampere Computing</h2>
<p>
Ampere utformar framtiden för hyperscalemoln och edge computing med
världens första molnprocessor. Byggd för molnet med en modern 64-bitars
ARM-serverbaserad arkitektur, ger Ampere kunderna friheten att accelerera
leverans av alla molnberäkningsapplikationer. Med branschledande molnprestanda,
effekteffektivitet och skalbarhet är Amperes processorer skräddarsydda för
den fortsatta tillväxten inom moln- och edge computing.
</p>

<h2>Om Debian</h2>

<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella 
	operativsystemet</q>.
</p>


<h2>Kontaktinformation</h2>

<p>
	För mer information, besök vänligen Debians webbplats på
	<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på 
	engelska) till &lt;press@debian.org&gt;.
</p>
