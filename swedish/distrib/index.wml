#use wml::debian::template title="Få tag på Debian"
#use wml::debian::translation-check translation="128241c9727552589d968997e05c3557e016c9c6"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian distribueras
<a href="../intro/free">fritt</a> över internet.
Du kan hämta allting från någon av våra
<a href="ftplist">speglar</a>.
<a href="../releases/stable/installmanual">Installationsguiden</a>
innehåller detaljerade instruktioner för installation. Och versionsfakta
kan hittas <a href="../releases/stable/releasenotes">här</a>.

</p>

<p>Denna sida har alternativ för att installera stabila utgåvan av Debian.
Om du är intresserad av uttestningsutgåvan (testing) eller instabila utgåvan
(unstable), besök vår sida med <a href="../releases/">versionsfakta</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Hämta en installeraravbildning</a></h2>
    <p>Beroende på din internetuppkoppling kan du hämta en av följande:</p>
    <ul>
      <li>En <a href="netinst"><strong>liten installeraravbildning</strong></a>:
		 kan hämtas snabbt från internet och bör skrivas till en CD. För
		 att använda denna så behöver du en maskin med en internetuppkoppling
		 tillgänglig.
	<ul class="quicklist downlist">
	  <li><a title="Hämta installationsprogrammet för 64-bitars Intel- och AMD-maskiner"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bitars
	      PC netinst iso</a></li>
	  <li><a title="Hämta installationsprogrammet för normala 32-bitars Intel- och AMD-maskiner"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bitars
	      PC netinst iso</a></li>
	</ul>
      </li>
      <li>En större <a href="../CD/"><strong>komplett 
		installeraravbildning</strong></a>: innehåller fler paket, gör det lättare att installera
		på maskiner utan tillgång till internet.
	<ul class="quicklist downlist">
	  <li><a title="Hämta DVD-torrents för 64-bitars Intel- och AMD-maskiner"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bitars PC torrents (DVD)</a></li>
	  <li><a title="Hämta DVD-torrents för normala 32-bitars Intel- och AMD-maskiner"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bitars PC torrents (DVD)</a></li>
	  <li><a title="Hämta CD-torrents för 64-bitars Intel- och AMD-maskiner"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC torrents (CD)</a></li>
	  <li><a title="Hämta CD-torrents för normala 32-bit Intel- och AMD-maskiner"
		 href="<stable-images-url/>/i386/bt-cd/">32-bitars PC torrents (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Använd en av Debians molnavbildningar</a></h2>
    <ul>
      <li>En officiell <a href="https://cloud.debian.org/images/cloud/"><strong>molnavbildning</strong></a>:
            kan användas direkt hos din molnleverantör, byggd av Debians Molngrupp.

        <ul class="quicklist downlist">
          <li><a title="OpenStack-avbildning för 64-bitars Intel and AMD Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">64-bitars AMD/Intel OpenStack (Qcow2)</a></li>
          <li><a title="OpenStack-avbildning för 64-bitars ARM Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">64-bitars ARM OpenStack (Qcow2)</a></li>
        </ul>
      </li>
    </ul>
    <h2><a href="../CD/live/">Pröva Debian innan du installerar</a></h2>
    <p>
	   Du kan prova Debian genom att starta ett live-system från en CD, DVD eller
		ett USB-minne utan att installera några filer på din dator. När du är klar
		kan du köra det inkluderade installationsprogrammet (med början i Debian
		10 Buster är detta den användarvänliga <a href="https://calamares.io">\
		Calamares-installeraren</a>). Förutsatt att installeraravbildningen
		uppfyller dina krav så kan den här metoden vara lämplig för dig.
		Läs mer <a href="../CD/live#choose_live">information om den här metoden</a>
      för att hjälpa dig ta ett beslut.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Hämta live torrents for 64-bitars Intel- and AMD-maskiner"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-bitars PC live torrents</a></li>
      <li><a title="Hämta live torrents for normala 32-bitars Intel- and AMD-maskiner"
	     href="<live-images-url/>/i386/bt-hybrid/">32-bitars PC live torrents</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Köpa en cd- eller dvd-uppsättning från en av de
      distributörer som säljer Debian-cd</a></h2>

  <p>
   Många distributörer säljer distributionen för mindre än femtio kronor
   eller fem euro plus frakt (se deras webbsidor för att se om de säljer
   internationellt).
   <br />
   Några av <a href="../doc/books">böckerna om Debian</a> levereras
   även med cd.
  </p>

  <p>
   Här är de grundläggande fördelarna med cd-uppsättningarna:
  </p>

  <ul>
   <li>Installation från cd-uppsättningarna är mer okomplicerat.</li>
   <li>Du kan installera på maskiner utan internetanslutning.</li>
   <li>Du kan installera Debian (på hur många maskiner du vill) utan att
       hämta alla paket själv.</li>
   <li>Cd:n kan lättare användas för att rädda ett skadat Debiansystem.</li>
  </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Köpa en dator med Debian
      förinstallerat</a></h2>
  <p>
   Det finns flera fördelar med detta:
  </p>

  <ul>
   <li>Du behöver inte installera Debian.</li>
   <li>Installationen är förkonfigurerad för att passa med maskinvaran.</li>
   <li>Återförsäljaren kan ge teknisk support.</li>
  </ul>
  </div>
</div>

<div id="firmware_nonfree" class="important">
<p>
Om någon av hårdvaran i ditt system <strong>kräver att icke-fria fastprogram
(firmware) läses in</strong> tillsammans med drivrutinen kan du använda en av
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarbollarna med vanliga fastprogrampaket</a> eller hämta en <strong>inofficiell</strong>
avbildning som inkluderar dessa <strong>icke-fria</strong> fastprogram.
Instruktioner om hur du använder tarbollarna och allmän information om hur
du läser in fastprogram under en installation kan hittas i
<a href="../../releases/stable/amd64/ch06s04">Installationsguiden</a>.</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">inofficiella
installationsavbildningfar för <q>stabila</q> utgåvan med fastprogram
inkluderade</a>
</p>
</div>


