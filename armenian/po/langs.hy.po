msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:28+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "Արաբերեն"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "Հայերեն"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "Ֆիներեն"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "Խորվաթերեն"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "Դանիերեն"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "Հոլանդերեն"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "Անգլերեն"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "Ֆրանսերեն"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "Գալիսերեն"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "Գերմաներեն"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "Իտալերեն"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "Ճապոներեն"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "Կորեերեն"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "Իսպաներեն"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "Պորտուգալերեն"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "Պորտուգալերեն (Բրազիլական)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "Չինարեն"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "Չինարեն (Չինաստան)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "Չինարեն (Հոն Կոնգ)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "Չինարեն (Թայվան)"

#: ../../english/template/debian/language_names.wml:28
#, fuzzy
msgid "Chinese (Traditional)"
msgstr "Չինարեն (Չինաստան)"

#: ../../english/template/debian/language_names.wml:29
#, fuzzy
msgid "Chinese (Simplified)"
msgstr "Չինարեն (Չինաստան)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "Շվեդերեն"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "Լեհերեն"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "Նորվեգերեն"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "Թուրքերեն"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "Ռուսերեն"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "Չեխերեն"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "Էսպերանտո"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "Հունգարեն"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "Ռումիներեն"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "Սլովակերեն"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "Հունարեն"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "Կատալեներեն"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "Ինդոնեզերեն"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "Լիտվերեն"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "Սլովեներեն"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "Բուլղարերեն"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "Թամիլերեն"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "Աֆրիկերեն"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "Ալբաներեն"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "Աստուրերեն"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "Ամհարերեն"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "Ադրբեջաներեն"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "Բասկերեն"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "Բելառուսերեն"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "Բենգալերեն"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "Բոսներեն"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "Բրետոներեն"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "Կոռներեն"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "Էստոներեն"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "Ֆարյորերեն"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "Գելական շոտլանդերեն"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "Վրացերեն"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "Եբրայերեն"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "Հնդկերեն"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "Իսլանդերեն"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "Ինտերլինգվա"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "Իռլանդերեն"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "Գրենլանդերեն"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "Կանադերեն"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "Քռդերեն"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "Լատվիերեն"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "Մակեդոներեն"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "Մալայերեն"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "Մալայալամ"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "Մալթերեն"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "Մանսկերեն"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "Մաորի"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "Մոնղոլերեն"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "Նորվեգերեն Bokm&aring;l"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "Նոր Նորվեգերեն"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "Օքսիտաներեն (post 1500)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "Պարսկերեն"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "Սերբերեն"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "Սլովեներեն"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "Տաջիկերեն"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "Թայերեն"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "Թոնգերեն"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "Ական"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "Ուկրաիներեն"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "Վիետնամերեն"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "Վալերեն"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "Կոսա"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "Իդիշ"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr ""
