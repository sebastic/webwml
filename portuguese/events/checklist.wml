#use wml::debian::template title="Checklist para um estande"
#use wml::debian::translation-check translation="96da98ca0d7c98bdddb5d60c39c8e06987402075"

<p>Esta lista pretende auxiliar as pessoas a organizarem um estande para o
projeto Debian em uma exposição. Por favor envie comentários para <a
href="mailto:events@debian.org">events@debian.org</a>.
</p>

<h3>Organizando um estande</h3>

<p>Esta lista é ordenada. Porém, alguns itens, é claro, podem ser feitos em
paralelo, e alguns outros podem ser feitos em ordens diferentes.

<ol>

<li> Se você tem conhecimento de um evento no qual o Debian pode participar,
     mande um e-mail para <a href="eventsmailinglists">debian-events-&lt;region&gt;</a> -
     isto também vale mesmo se você não estiver planejando organizar um estande.
</li>

<li> Para eventos no Brasil, por favor, envie um e-mail para a lista de discussão
     <a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos">debian-br-eventos</a>
</li>

<li> Organizar um estande só faz sentido se existem duas ou mais pessoas dividindo
     as tarefas. Considere pedir ajuda em <a
     href="eventsmailinglists">debian-events-&lt;region&gt;</a> se você não está
     recebendo ajuda suficiente. Se ninguém mais quer ou pode te ajudar, por favor
     reconsidere a ideia de organizar um estande.
</li>

<li> Agora que existem algumas pessoas que irão ajudar no estande, tenha certeza
     que vocês tem pelo menos uma máquina disponível no estande para mostrar o
     Debian - como uma alternativa, a caixa de eventos Debian está disponível para
     isso. Pergunte em events@debian.org. Se não existe nenhuma máquina disponível,
     por favor reconsidere organizar um estande, uma vez que sem máquinas o
     estande ficará bem vazio.
</li>

<li> Agora que pessoas e máquinas estão disponíveis, é hora de entrar em
     contato com a organização do evento e perguntar sobre a disponibilidade de
     um estande gratuito para o projeto.
</li>

<li> Tem se mostrado útil coordenar os eventos na Wiki do Debian
     usando o formulário disponível na <a
     href="https://wiki.debian.org/DebianEvents#Adding_a_new_event">página de
     eventos do Debian</a>.
</li>

<li> Por favor certifique-se que terão móveis no estande.
</li>

<li> Por favor certifique-se de que o estande terá uma tomada de energia e conexão
     com a internet.
</li>

<li> Verifique se existirão paredes para pendurar os banners.
</li>

<li> Caso esteja disponível sem custo ou a um custo baixo, peça para ter conexão
     de internet, caso você considere que isso seja útil para o estande.
</li>

<li> Caso banners possam ser usados, verifique se você pode arcar com os
     custos de imprimir banners grandes, ou peça para enviarem alguns para você
     (confira o <a href="material">materiais para estandes</a>). O Debian também
     pode reembolsar os custos para imprimir banners grandes.
</li>

<li> Confira se você pode arcar com os custos de formulação, design e impressão
     de panfletos para que os visitantes possam levar algo do estande
     (confira o <a href="material">materiais para estandes</a>).
</li>

# <-- <li> Caso seja uma grande exposição, confira se alguém quer produzir CDs Debian
#     para visitantes. Confira se você pode encontrar patrocinadores para
#     pagar pela produção para que você possa oferecê-los gratuitamente.
#     Certifique-se de que a entrega de CDs binários esteja realmente de
#     <a href="$(HOME)/CD/vendors/legal">acordo</a> com a licença de
#     distribuição de software.
#</li>
# rewritten. -->

<li> Caso alguém no estande seja capaz de dar uma palestra
     sobre o Debian em geral ou sobre um aspecto específico do projeto,
     peça para ele(a) falar com a organização do evento para ver se aceitam a
     palestra. De qualquer forma, você pode sempre agendar palestras no estande,
     mas certifique-se de publicar a agenda previamente (no primeiro dia) para
     que as pessoas tenham conhecimento disso.
</li>

<li> Mesmo que nenhuma palestra formal esteja sendo organizada, você pode
     sempre pedir para a organização do evento uma sala para realizar um encontro
     <q>Birds of a Feather (BoF)</q>. Um BoF é um encontro informal entre
     desenvolvedores(as) e usuários(as) no qual qualquer pessoa pode perguntar
     qualquer questão na qual esteja interessada. Eles são bastante elucidantes
     porque os(as) desenvolvedores(as) têm a chance de saber o que as pessoas pensam
     e as pessoas obtém respostas às dúvidas que possuem.
</li>

<li> Negocie com a organização do evento quantas mesas e cadeiras forem
     necessárias para o estande.
</li>

<li> Por favor certifique-se que no estande existirão réguas com tomadas de
     energia o suficiente para que todas as máquinas possam ser ligadas.
</li>

<li> Por favor certifique-se que no estande existirão cabos de rede e
     hubs/switches suficientes para que as máquinas possam ser conectadas.
</li>

<li> Mesas descobertas não dão uma boa impressão, você pode considerar usar
     toalhas de mesa brancas para que o estande inteiro pareça mais profissional.
</li>

<li> Se você quer vender alguma coisa (camisetas, cds, pins, bonés, etc),
     você deverá primeiramente contatar a organização do evento para verificar
     se é permitido. Há grandes eventos onde não é permitido vender produtos
     nos estandes.
</li>

<li> Se a organização do evento permitir que você venda produtos no estande,
     você deve verificar ainda se há alguma lei local ou estadual que
     proíba isso (especialmente se o evento for realizado em um domingo).
     A organização do evento talvez possa ajudar a verificar isso.
</li>

<li> Se você está vendendo produtos, você não deve enfatizar que está ganhando
     dinheiro no estande. Pode parecer estranho você pedir para a organização
     do evento um estande gratuito, etc, e agora está vendendo produtos e
     ganhando <q>muito</q> dinheiro nele.
</li>

<li> Em alguns eventos, é organizado um evento social para palestrantes e
     expositores. Confira se haverá um, e como você pode estar presente se
     estiver interessado(a). Como uma regra geral: um evento social é bem
     legal uma vez que você pode encontrar pessoas de outros projetos e
     empresas, e pode relaxar fora de uma exposição cansativa.
</li>

<li> Por favor certifique-se de levar <q>fingerprints</q> suficientes da sua chave
     GnuPG para que outros(as) possam <a href="keysigning">assiná-la</a>.
     Por favor não se esqueça também do passaporte ou da licença de motorista
     para provar sua identidade.
</li>

<li> Caso você tenha dinheiro para fazer mais pelo estande, você pode ler algumas
     boas ideias com relação a isso em
     <a href="material">merchandising em estandes</a>.

<li> Quando as pessoas e as máquinas estiverem disponíveis, certifique-se de
     que toda a equipe do estande pode operar as máquinas. Isso requer uma
     conta com uma senha conhecida por todo o grupo. Essa conta deve poder
     executar <code>sudo apt-get</code> se as máquinas estiverem conectadas a
     uma rede e tiverem acesso a um espelho do Debian.
</li>

<li> Será legal se uma das pessoas que estão na equipe do estande
     puder escrever um pequeno relatório depois do evento, assim a
     <a href="$(HOME)/News/weekly/">Debian Weekly News</a> poderá divulgar
     a participação no evento.
</li>

</ol>

<p>Mensagens a respeito da organização do estande e palestras podem ser
enviadas para uma das <a href="booth#ml">listas de discussão</a> assim elas
serão publicamente conhecidas, arquivadas e podem atrair mais pessoas
interessadas em comparecer.
</p>

<p>Caso você não queira enviar tais mensagens para uma lista pública, você
pode sempre enviá-las para events@debian.org para que possamos lhe dar
algumas dicas, caso seja necessário ou útil. Porém nós não entraremos em
contato com a organização do evento sobrepondo os seus esforços.

<h3>Organizando o estande</h3>

<ul>

<li> Caso você queira organizar um estande profissional, é sempre bom
     saber onde colocar casacos, calçados supérfluos, bebidas e outras
     coisas. Por favor entre em contato com a organização caso exista
     uma pequena sala para isso.
</li>

<li> Por favor verifique com a organização do evento como você pode acessar o
     estande antes que o mesmo tenha sido aberto para o público.
     Algumas exposições requerem credenciais especiais de expositores.
</li>

<li> Na exposição a média de desenvolvedores(as) por visitante não deve ser
     maior de (2.0). É legal ter um estande cheio de pessoas, mas não é
     muito convidativo se o estande já estiver lotado de pessoas do Debian
     e nenhum(a) visitante puder entrar.
</li>

<li> Não esqueça de tirar uma foto em grupo de todas as pessoas do Debian
     que estão ajudando. Isto geralmente é esquecido, mas associar fotos e
     pessoas ajuda bastante, ao mesmo tempo em que faz com que o Debian
     seja menos anônimo.
</li>

<li> No final do dia ou no final do evento, por favor não devolva um estande
     cheio de lixo como garrafas vazias e outras coisas.
</li>

<li> Não parecerá legal se as pessoas no estande estiverem sentadas atrás da mesa
     apenas olhando seus notebooks ao invés de se preocuparem com os(as) visitantes.
</li>

</ul>
