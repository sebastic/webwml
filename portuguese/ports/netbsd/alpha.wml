#use wml::debian::template title="Debian GNU/NetBSD para Alpha" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#include "$(ENGLISHDIR)/ports/netbsd/menu.inc"
#use wml::debian::translation-check translation="393cfc4091e024ab88ffcf6a79fadb87704d3464"
{#style#:
<style type="text/css">
    pre.input {
	margin-left:	5%;
    }
</style>
:##}

<div class="important">
<p><strong>
Este trabalho de porte foi abandonado faz tempo. Ele não tem atualizações desde
outubro de 2002. A informação nesta página é somente para propósitos históricos.
</strong></p>
</div>


<h1>Debian GNU/NetBSD para Alpha</h1>


<h2>Estado</h2>

<p>Este porte atualmente está em um estágio muito preliminar. Atualmente, ele
não consegue fazer boot por si mesmo, mas um chroot de construção, que está
hospedado em um sistema NetBSD-alpha nativo, está configurado. Alguns pacotes
básicos já estão feitos, e agora é até possível construir alguns pacotes com:</p>

<pre class="input">
    (chroot)# dpkg-source -x package.dsc
    (chroot)# cd package-*
    (chroot)# dpkg-buildpackage -d -us -uc
</pre>

<p>Contudo, o chroot para construir esses pacotes ainda está usando a maioria
das bibliotecas do NetBSD (em particular, seu libc), junto com gcc e
binutils. Um objetivo secundário atual é construir um tool chain Debian
<em>nativo</em> totalmente funcional. Existem alguns <a
href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">problemas</a>
com isso, então qualquer ajuda é muito apreciada.</p>


<h2>Pacotes disponíveis</h2>

<p>A maior parte dos pacotes binários produzidos até agora compilaram com
poucas ou nenhuma mudanças, embora algumas funcionalidades tiveram que ser
desligadas por enquanto devido à ausência de dependências de construção.</p>

<verbatim>
||/ Nome              Versão                Descrição
+++-=================-=====================-==================================================================
ii  autoconf2.13      2.13-39               construtor automático de script configure (versão obsoleta)
ii  automake          1.4-p4-1.1            Uma ferramenta para gerar Makefiles em conformidade com o padrão GNU
ii  autotools-dev     20020102.1            Infraestrutura atualizada para arquivos config.{guess,sub}
ii  bison             1.32-1                Gerador de analisador compatível com YACC
ii  bzip2             1.0.1-14              Compressor de arquivos ordenador de blocos de alta qualidade - utilitários
ii  debconf           1.0.25                Sistema de gerenciamento de configuração Debian
ii  debconf-utils     1.0.25                Utilitários debconf
ii  debhelper         3.4.1                 Programas de ajuda para debian/rules
ii  debianutils       1.15                  Utilitários diversos específicos para o Debian
ii  dejagnu           1.4-4                 Framework para executar suítes de teste em ferramentas de software
ii  diff              2.7-28                Utilitários para comparação de arquivos
ii  dpkg              1.9.18                Sistema de gerenciamento de pacotes Debian
ii  dpkg-dev          1.9.18                Ferramenta de construção de pacotes Debian
ii  file              3.37-3                Reconhece o tipo de dados em um arquivo utilizando números "mágicos"
ii  flex              2.5.4a-20             Rápido gerador de análise léxica
ii  flex-doc          2.5.4a-20             Documentação para o GNU flex
ii  gettext           0.10.40-1             Utilitários de internacionalização GNU
ii  gettext-base      0.10.40-1             Utilitários de internacionalização do GNU para o sistema base
ii  groff             1.17.2-16             Sistema de formatação de textos GNU troff
ii  groff-base        1.17.2-16             Sistema de formatação de texto troff GNU (componentes do sistema base)
ii  gzip              1.3.2-3               Utilitários de compactação GNU
ii  hostname          2.09                  Utilitário para atribuir/mostrar o nome da máquina ou o nome do domínio
ii  info              4.0b-2                Visualizador independente de documentação do GNU Info
ii  libbz2-1.0        1.0.1-14              Uma biblioteca compressora de arquivos de alta qualidade e com ordenação de block - execução
ii  libbz2-dev        1.0.1-14              Uma biblioteca compressora de arquivos de alta qualidade e com ordenação de block - desenvolvimento
ii  m4                1.4-14                Linguagem de processamento de macros
ii  patch             2.5.4-6               Aplica um arquivo diff a um original
ii  tar               1.13.25-1             GNU tar
ii  texi2html         1.64-cvs20010402-2    Converte arquivos Texinfo para HTML
ii  texinfo           4.0b-2                Sistema de documentação para informações online e saída impressa
ii  wget              1.7-3                 Utilitário para recuperação de arquivos da WWW via HTTP e FTP
ii  zlib-bin          1.1.3-19              Biblioteca de compressão - programas de amostra
ii  zlib1g            1.1.3-19              Biblioteca de compressão - execução
ii  zlib1g-dev        1.1.3-19              Biblioteca de compressão - desenvolvimento
</verbatim>


<h2>Recursos</h2>

<h3>Pacotes</h3>

<p>
Atualmente, nem os pacotes binários, nem o chroot de construção estão
disponíveis on-line, mas logo estarão.
</p>

<h3>Correções</h3>

<ul>
<li><a href="https://people.debian.org/~michaelw/debian-netbsd.patch">
correção rápida e 'suja'</a> para fazer o <a href="https://packages.debian.org/dpkg">
dpkg</a> compilar.  Com gambiarras adicionais no
<code>INCLUDE_PATH</code> e <code>LDFLAGS</code>, ele até compila
em um sistema NetBSD <em>nativo</em>.
</li>
</ul>

<h2>Contato</h2>

<p>Para informações adicionais, por favor contate (em inglês) <a
href="mailto:michaelw@debian.org?subject=Debian%20GNU/NetBSD%20Alpha">
Michael Weber</a>.</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
