#use wml::debian::cdimage title="Debian cd-images downloaden met BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

# Last Translation update by: $Author$
# Last Translation update at: $Date$

<p><a href="https://nl.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
is een peer-to-peer downloadsysteem geoptimaliseerd voor grote aantallen
gebruikers. Het zet een minimale belasting op onze servers omdat
BitTorrent-clients stukken van bestanden uploaden naar anderen terwijl ze
downloaden, dus wordt de belasting gespreid over het netwerk en maakt het
ontzettend snelle downloads mogelijk.
</p>

<div class="tip">
<p>Het <strong>eerste</strong> cd/dvd-image bevat alle bestanden die nodig
zijn voor de installatie van een standaard Debian systeem.<br />
Download om onnodige belasting van onze servers te voorkomen alstublieft
<strong>geen</strong> andere cd- of dvd-images, tenzij u weet dat u de
pakketten daarop nodig heeft.</p>
</div>

<p>
U hebt een BitTorrent-client nodig om via deze methode cd- en dvd-images van
Debian te downloaden. Binnen de Debian distributie zijn beschikbaar:
<a href="https://packages.debian.org/bittornado">BitTornado</a>,
<a href="https://packages.debian.org/ktorrent">KTorrent</a> en het originele
<a href="https://packages.debian.org/bittorrent">BitTorrent</a>.
Andere besturingssystemen worden ondersteund door:
<a href="http://www.bittornado.com/download.html">BitTornado</a> en
<a href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>


<h3>Officiële torrents voor de <q>stable</q> release</h3>

<div class="line">
<div class="item col50">
<p><strong>cd</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>dvd</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>


<p>Bekijk ook de beschikbare documentatie voor u een installatie begint.
<strong>Als u slechts één document wilt lezen</strong> voor de installatie,
lees dan onze
<a href="$(HOME)/releases/stable/i386/apa">Installatie Howto</a>, een kort
overzicht van het installatieproces. Andere nuttige documentatie:
</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installatiehandleiding</a>,
    bevat gedetaileerde installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentatie over het
    Debian Installatiesysteem</a>, waaronder antwoorden op veel gestelde vragen
   (FAQ)</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata bij het
    Debian Installeratiesysteem</a>, een overzicht van bekende problemen in het
    installatiesysteem</li>
</ul>

#<h3>Officiële torrents voor de <q>testing</q> distributie</h3>
#
#<ul>
#  <li><strong>cd</strong>:<br>
#  <full-cd-torrent>
#  </li>
#
#  <li><strong>dvd</strong>:<br>
#  <full-dvd-torrent>
#  </li>
#</ul>

<p>
Laat indien mogelijk na afloop van de download uw BitTorrent client actief
om anderen te helpen de images sneller te downloaden!
</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Als op uw systeem bepaalde hardware <strong>vereist dat niet-vrije firmware
geladen wordt</strong> samen met het stuurprogramma voor het apparaat, kunt u
een van de <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">tar-archieven met gangbare firmware-pakketten</a>
gebruiken of een <strong>niet-officieel</strong> image downloaden dat deze
<strong>niet-vrije</strong> firmware bevat. Instructies over het gebruik van
tar-archieven en algemene informatie over het laden van firmware tijdens een
installatie kunt u vinden in de
<a href="../../releases/stable/amd64/ch06s04">Installatiehandleiding</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">niet-officiële installatie-images voor de release
<q>stable</q> die firmware bevatten</a>
</p>
</div>

