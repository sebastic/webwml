#use wml::debian::template title="Hoe u aan de webpagina's van Debian kunt werken" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="b5c35a479efa1910b978df5114fd87e4142e3892"

<toc-display/>



<toc-add-entry name="general">Algemene informatie</toc-add-entry>

<h3>Vereiste bronnen</h3>

<p>Als u aan onze website wilt werken, wees dan bereid om ten minste 740 MB aan
gegevens op uw schijf op te slaan. Dit is de huidige grootte van het
bronarchief. Als u (per ongeluk) alle pagina's opnieuw opbouwt, zult u minstens
drie keer zoveel ruimte nodig hebben.</p>

<h3><q>Wat betekenen de lijnen die beginnen met `#'?</q></h3>

<p>In WML is een regel die begint met `#', een commentaar. Dit geniet de
voorkeur boven normale HTML-commentaar, omdat dit op de uiteindelijke pagina
niet te zien is.</p>

<p>Lees de pagina over <a href="using_wml">Het gebruik van WML</a> voor meer
informatie over WML.</p>

<toc-add-entry name="etiquette">Etiquette voor redacteuren</toc-add-entry>

<h3><q>Kan ik deze pagina aanpassen?</q></h3>

<p>Dat hangt ervan af. Als u een kleine fout ziet, zoals een typefout, repareer
deze dan gewoon.
</p>

<p>Als u merkt dat er een stukje informatie ontbreekt, kunt u dit ook
repareren.</p>

<p>Als u vindt dat iets verschrikkelijk is en herschreven moet worden, breng
dit dan ter sprake op debian-www, zodat het besproken kan worden. We zullen het
waarschijnlijk met u eens zijn.</p>

<p>Als u merkt dat er een probleem is in een sjabloon (d.w.z. een bestand in de
map webwml/english/template/debian), denk dan alstublieft na over de wijziging
voordat u deze doorvoert, omdat wijzigingen in sjablonen er vaak voor zorgen
dat grote delen van de site opnieuw moeten worden opgebouwd.</p>

<h3>Als u een nieuwe map toevoegt, voeg dan ook de Makefile toe!</h3>

<p>Er moet enige voorzichtigheid in acht genomen worden als u een nieuwe map
toevoegt aan git. Als de huidige map in ../Makefile vermeld staat, dan
<b>moet</b> u er een Makefile in maken &mdash; anders zal <tt>make</tt> een
foutmelding geven.</p>

<h3>Gebruik duidelijk en eenvoudig Engels</h3>

<p>Aangezien de Debian-webpagina's worden gelezen door personen waarvoor Engels
niet de moedertaal is, en aangezien ze vertaald worden in andere talen, doet u
er best aan om in duidelijk en eenvoudig Engels te schrijven en het gebruik van
straattaal, emoticons en obscure uitdrukkingen te vermijden.
</p>

<p>Als u toch iets dergelijks gebruikt, voeg dan een commentaar toe aan het
bestand waarin u de betekenis uitlegt.</p>

<p>
Neem bij twijfel of om uw voorstel te laten nalezen contact op met het
<a href="mailto:debian-l10n-english@lists.debian.org">Engelse lokalisatieteam</a>.
</p>

<h3>Zoek naar README's</h3>

<p>Sommige mappen bevatten een README om u te helpen begrijpen hoe die map is
georganiseerd. Dit zou alle speciale informatie moeten geven die nodig is bij
het werken in dat onderdeel.</p>

<h3>Scheid de wijzigingen aan de inhoud van de wijzigingen aan de opmaak</h3>

<p>Maak altijd aparte patches of vastleggingen voor inhoudelijke wijzigingen en
voor wijzigingen aan de opmaak. Als ze worden gecombineerd, is het voor
vertalers veel moeilijker om de verschillen te vinden. Als u
<kbd>git diff -u</kbd> uitvoert met zulke gemengde wijzigingen, kunt u zelf de
puinhoop zien.</p>

<p>Vermijd in het algemeen willekeurige wijzigingen aan de opmaak. Het
XHTML/XML-compatibel maken van oudere delen van pagina's zou niet in dezelfde
vastlegging mogen gebeuren als andere wijzigingen. (Nieuwe dingen kunnen en
moeten natuurlijk vanaf het begin goed worden gedaan.)</p>


<h3>Werk indien mogelijk ook de vertalingen bij</h3>

<p>Sommige wijzigingen zijn onafhankelijk van de taal die in een WML-bestand
wordt gebruikt, zoals wijzigingen aan URL's of aan ingebedde Perl-code. Het
corrigeren van typefouten valt ook in dezelfde categorie, omdat vertalers ze
meestal hebben genegeerd tijdens het vertalen. Met dergelijke
taal-onafhankelijke wijzigingen kunt u dezelfde wijziging aanbrengen in alle
vertaalde bestanden zonder de andere talen echt te kennen, en kunt u veilig de
versie verhogen in de kopregel translation-check.</p>

<p>Het is niet erg moeilijk voor vertalers om dit werk zelf te doen, en het kan
voor Engelstalige redacteuren ongemakkelijk zijn om te werken met een volledige
kopie van de broncode van de website. Toch moedigen wij mensen aan om dit wel
te doen om te voorkomen dat twee dozijn mensen lastiggevallen worden voor iets
dat snel door één persoon kan worden gedaan.</p>

<p>Om het makkelijker te maken om dergelijke wijzigingen toe te passen, kunt u
bovendien het script
<a href="using_git#translation-smart-change"><code>smart_change.pl</code></a>
gebruiken uit de hoofdmap van de git-module webwml.</p>

<toc-add-entry name="links">Links</toc-add-entry>

<h3><q>Deze link ziet er niet goed uit. Moet ik hem aanpassen?</q></h3>

<p>Door de manier waarop de webservers zijn ingesteld (met behulp van
<a href="content_negotiation">onderhandeling over de inhoud</a>) zou het niet
mogen zijn dat u een van de interne links moet wijzigen. We raden u zelfs aan
om dat niet te doen. Schrijf naar debian-www als u denkt dat een link onjuist
is voordat u hem verandert.</p>

<h3>Links repareren</h3>

<p>Als u merkt dat een link naar een externe website resulteert in een omlegging
(301, 302, een &lt;meta&gt;omlegging, of een pagina met daarop <q>Deze pagina
is verhuisd.</q>), rapporteer dit dan op debian-www.</p>

<p>Als u een defecte link (404, 403, of een pagina die niet is wat de link
zegt) vindt, repareer deze dan en signaleer het op debian-www, zodat vertalers
hiervan op de hoogte zijn. Nog beter: repareer de link in alle andere
vertalingen en werk indien mogelijk de koptekst translation-check bij.</p>

<toc-add-entry name="special">Gescheiden tekst en data</toc-add-entry>

<h3><q>Wat is de betekenis van bestanden zoals foo.def en foo.data?</q></h3>

<p>Om de vertalingen gemakkelijker up-to-date te houden, scheiden we de
generieke delen (data) van de tekstuele delen (tekst) van sommige pagina's.
Vertalers moeten alleen de tekstuele delen kopiëren en vertalen. De generieke
delen worden automatisch toegevoegd.</p>

<p>Een voorbeeld kan helpen om dit te begrijpen. Er zijn verschillende
bestanden nodig om de pagina met leveranciersvermeldingen in
<code>CD/vendors</code> te genereren:</p>

<dl>
  <dt><code>index.wml</code>:</dt>
      <dd>De tekst bovenaan de leverancierspagina staat in dit bestand.
      Een vertaalde kopie hiervan moet in elke taalmap worden geplaatst.</dd>
  <dt><code>vendors.CD.def</code>:</dt>
      <dd>Dit bevat alle stukken tekst die nodig zijn voor elk van de
      leveranciersitems. Vertalingen ervan worden toegevoegd via
      <code>&lt;<var>taal</var>&gt;/po/vendors.<var>xy</var>.po</code>.</dd>
  <dt><code>vendors.CD</code>:</dt>
      <dd>Dit bestand bevat de feitelijke leverancierselementen die niet
      taalafhankelijk zijn. Een vertaler moet dit bestand dus niet aanraken.</dd>
</dl>

<p>Wanneer een van de mensen achter <email "cdvendors@debian.org"> een nieuwe
leverancier toevoegt, voegt die deze toe aan <code>debiancd.db</code>, zet dit
dan om naar een WML-indelling als <code>vendors.CD</code> (met behulp van
<code>getvendors.pl</code>), en laat dan WML en de makefiles hun werk doen.
Alle vertalingen worden opnieuw opgebouwd met de bestaande vertaalde tekst en
met de nieuwe leveranciersgegevens. (En zie, zomaar een bijgewerkte
vertaling!)</p>

<toc-add-entry name="newpage">Een nieuwe pagina toevoegen</toc-add-entry>

<p>Nieuwe pagina's toevoegen aan Debian is vrij eenvoudig. Al het werk om de
kop- en voettekst goed te krijgen, wordt gedaan met WML. Het enige wat u moet
doen, is bovenaan het nieuwe bestand een regel plaatsen zoals de volgende:</p>

<pre><protect>
#use wml::debian::template title="PAGINATITEL"
</protect></pre>

<p>gevolgd door de eigenlijke tekst. Alle pagina's moeten het sjabloonbestand
<code>wml::debian::template</code> gebruiken, tenzij zij van een bijzonder
sjabloonbestand gebruik maken dat enkel voor die sectie gemaakt werd, bijv.
voor de nieuws- of voor de beveiligingsitems.</p>

<p>Met de sjablonen die we hebben, kunt u bepaalde variabelen definiëren die
van invloed zijn op de gemaakte pagina's. Dit zou moeten voorkomen dat voor
elke situatie een verschillend sjabloon moet worden gemaakt en zou er moeten
voor zorgen dat verbeteringen eenvoudiger kunnen worden geïmplementeerd. De
momenteel beschikbare variabelen en hun doel zijn:</p>

<dl>
<dt>BARETITLE="true"</dt>
	<dd>Verwijdert het onderdeel "Debian --" dat meestal voor alle
	&lt;title&gt;-tags geplaatst wordt.</dd>
<dt>NOHEADER="true"</dt>
	<dd>Verwijdert de initiële koptekst van de pagina. Een aangepaste koptekst
    kan natuurlijk in de eigenlijke tekst worden opgenomen.</dd>
<dt>NOMIRRORS="true"</dt>
	<dd>Verwijdert de vervolgkeuzelijst met spiegelservers van de pagina. Het
    gebruik ervan wordt over het algemeen afgeraden, behalve voor een handvol
    pagina's.</dd>
<dt>NOHOMELINK="true"</dt>
	<dd>Verwijdert de link die terugvoert naar de hoofdpagina van Debian en die
    normaal onderaan de pagina staat.</dd>
<dt>NOLANGUAGES="true"</dt>
	<dd>Verwijdert de links naar versies in andere talen, die normaal onderaan
    de pagina staan.</dd>
<dt>GEN_TIME="true"</dt>
	<dd>Stelt de datum op de resulterende bestanden in op de tijdstempel van de
    gegenereerde bestanden, in plaats van op de tijdstempel van het
    bronbestand.</dd>
<dt>NOCOPYRIGHT="true"</dt>
	<dd>Verwijdert de copyrightvermelding onderaan de pagina.</dd>
</dl>

<p>Merk op dat u elke tekenreeks kunt gebruiken als waarde voor deze
variabelen, <q>true</q>, <q>yes</q>, <q>foo</q>, het maakt niet uit.</p>

<p>Een voorbeeld van het gebruik hiervan zijn de pagina's over Debian voor
andere architecturen, die hun eigen kopteksten hebben.
<code>ports/arm/index.wml</code> gebruikt:</p>

<pre><protect>
#use wml::debian::template title="ARM Port" NOHEADER="yes"
</protect></pre>

<p>Indien u iets wilt doen dat niet mogelijk is met de bestaande sjablonen,
overweeg dan eerst om een van hen uit te breiden. Als het niet mogelijk is om
er een op een achterwaarts compatibele manier uit te breiden, probeer dan het
nieuwe sjabloon te maken als een superset van een bestaand sjabloon, zodat de
pagina's ernaar kunnen worden geconverteerd bij de volgende grote upgrade
(hopelijk nooit vaker dan om de 6 maanden).</p>

<p>Als u een pagina maakt die door een script wordt gegenereerd of weinig tekst
bevat, overweeg dan het gebruik van de &lt;gettext&gt;-tags om de taak van het
bijhouden van vertalingen te verlichten.</p>

# think of a good example for <gettext> in new pages

<toc-add-entry name="inclusion">Andere bestanden insluiten</toc-add-entry>

<p>Als u sommige delen van uw pagina in een afzonderlijk bestand wilt
onderbrengen (dat dan door uw hoofdbestand wordt ingesloten), gebruik dan de
extensie <code>.src</code> als uw bestand inhoud bevat die vertaald moet
worden, want dan wordt uw ingesloten bestand opgevolgd voor wijzigingen zoals
elk gewoon <code>.wml</code>-bestand. Als u een andere extensie gebruikt, zoals
<code>.inc</code>, zullen vertalers uw updates niet opmerken en kunnen
verschillende talen verschillende inhoud leveren.</p>

<toc-add-entry name="newdir">Een nieuwe map toevoegen</toc-add-entry>

<p>Opmerking: maak <strong>geen</strong> map met de naam
<code>install</code>. Dit brengt make in verwarring en de pagina's in die map zullen niet automatisch bijgewerkt worden.</p>

<p>Hieronder staat een geannoteerd voorbeeld van het toevoegen van een nieuwe
map aan de website.
</p>
<pre>
   mkdir foo
   git add foo
   cd foo
   cp ../intro/Makefile .
   git add Makefile
</pre>

<p>Bewerk in de bovenliggende map het bestand Makefile en voeg de map die u net
maakte, toe aan de variabele <code>SUBS</code>. Dit zal de map toevoegen aan de
op te bouwem onderdelen wanneer make wordt uitgevoerd.</p>

<p>Leg tenslotte alle wijzigingen die zojuist in het depot zijn
aangebracht, vast met
</p>
<pre>
  git commit Makefile foo
</pre>
