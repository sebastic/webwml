#use wml::debian::template title="Introducción a Debian" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h1>Debian es una comunidad</h1>
<p>Miles de voluntarios en todo el mundo trabajan juntos priorizando el software libre
y las necesidades de los usuarios.</p>

<ul>
  <li>
    <a href="people">Las personas:</a>
    quiénes somos, qué hacemos
  </li>
  <li>
    <a href="philosophy">Filosofía:</a>
    por qué y cómo lo hacemos
  </li>
  <li>
    <a href="../devel/join/">Implíquese:</a>
    ¡usted puede formar parte!
  </li>
  <li>
    <a href="help">¿Cómo puede ayudar a Debian?</a>
  </li>
  <li>
    <a href="../social_contract">Contrato social:</a>
    nuestro programa ético
  </li>
  <li>
    <a href="diversity">Declaración de diversidad</a>
  </li>
  <li>
    <a href="../code_of_conduct">Código de conducta</a>
  </li>
  <li>
    <a href="../partners/">Socios:</a>
    empresas y organizaciones que proporcionan asistencia continuada al proyecto Debian
  </li>
  <li>
    <a href="../donations">Donaciones</a>
  </li>
  <li>
    <a href="../legal/">Información legal</a>
  </li>
  <li>
    <a href="../legal/privacy">Privacidad de los datos</a>
  </li>
  <li>
    <a href="../contact">Contacto</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h1>Debian es un sistema operativo libre</h1>
<p>Comenzamos con Linux y agregamos miles de aplicaciones para cumplir con
  las necesidades de los usuarios.</p>

<ul>
  <li>
    <a href="../distrib">Descargas:</a>
    más variantes de las imágenes de Debian
  </li>
  <li>
  <a href="why_debian">Por qué Debian</a>
  </li>
  <li>
    <a href="../support">Soporte:</a>
    obtener ayuda
  </li>
  <li>
    <a href="../security">Seguridad:</a>
    última actualización <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">Paquetes de software:</a>
    buscar y navegar por la larga lista de nuestro software
  </li>
  <li>
    <a href="../doc">Documentación</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">Wiki de Debian</a>
  </li>
  <li>
    <a href="../Bugs">Informes de fallos</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Listas de correo</a>
  </li>
  <li>
    <a href="../blends">Mezclas («blends»):</a>
    Metapaquetes para necesidades específicas
  </li>
  <li>
    <a href="../devel">El rincón del desarrollador:</a>
    Información de interés para desarrolladores de Debian
  </li>
  <li>
    <a href="../ports"> Adaptaciones/Arquitecturas:</a>
    Arquitecturas de CPU que soportamos
  </li>
  <li>
    <a href="search">Información de cómo usar el motor de búsquedas de Debian</a>.
  </li>
  <li>
    <a href="cn">Información de páginas disponibles en varios idiomas</a>.
  </li>
</ul>
