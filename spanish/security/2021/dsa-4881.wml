#use wml::debian::translation-check translation="1ee140ecea4124e2849e2bc5c2de4b63b838e5c5"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en cURL, una biblioteca para transferencia de URL:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8169">CVE-2020-8169</a>

    <p>Marek Szlagor informó de que se podía hacer que libcurl añadiera
    una parte de la contraseña al nombre de máquina antes de resolver este,
    revelando, potencialmente, el fragmento de contraseña por la red y al
    servidor o servidores DNS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8177">CVE-2020-8177</a>

    <p>sn informó de que un servidor malicioso podía hacer que curl
    sobrescribiera un fichero local cuando se usaban las opciones -i
    (--include) y -J (--remote-header-name) en la misma línea de órdenes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8231">CVE-2020-8231</a>

    <p>Marc Aldorasi informó de que libcurl podría usar la conexión equivocada
    cuando una aplicación que utiliza la API multi de libcurl activa la opción
    CURLOPT_CONNECT_ONLY, lo que podría dar lugar a fugas de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

    <p>Varnavas Papaioannou informó de que un servidor malicioso podría usar la
    respuesta PASV para hacer que curl se conectara a una dirección IP
    y puerto arbitrarios, provocando, potencialmente, que curl extrajera información sobre
    servicios que, de otra manera, son privados y no son revelados.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

    <p>xnynx informó de que libcurl podría quedarse sin espacio en la pila cuando se utiliza
    la funcionalidad de búsqueda de coincidencias con caracteres comodín FTP (CURLOPT_CHUNK_BGN_FUNCTION).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

    <p>Se informó de que libcurl no verificaba que la respuesta OCSP
    correspondiera realmente al certificado cuyo estado se quería comprobar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22876">CVE-2021-22876</a>

    <p>Viktor Szakats informó de que libcurl no quita las credenciales
    del usuario de la URL cuando rellena automáticamente el campo de
    cabecera Referer en peticiones HTTP salientes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22890">CVE-2021-22890</a>

    <p>Mingtao Yang informó de que, cuando se utiliza un proxy HTTPS y TLS 1.3,
    libcurl podría malinterpretar los tickets de sesión provenientes del proxy HTTPS
    como si vinieran del servidor remoto. Esto podría permitir
    que un proxy HTTPS forzara a libcurl a utilizar el ticket de sesión equivocado
    para la máquina y, en consecuencia, a eludir la verificación del certificado TLS del servidor.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 7.64.0-4+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de curl.</p>

<p>Para información detallada sobre el estado de seguridad de curl, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4881.data"
