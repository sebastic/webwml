#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e"
<define-tag pagetitle>Debian 10 aktualisiert: 10.9 veröffentlicht</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die neunte Aktualisierung für seine stabile 
Distribution Debian <release> (codename <q><codename></q>) ankündigen zu dürfen. 
Diese Zwischenveröffentlichung bringt hauptsächlich Korrekturen für 
Sicherheitsprobleme und einige Anpassungen für einige ernste Probleme. Für 
sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden; auf 
diese wird, wo vorhanden, verwiesen.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, Buster-Medien zu entsorgen, da deren Pakete 
nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers auf den 
neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht
viele Pakete auf den neuesten Stand bringen müssen. Die meisten
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem das
Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>
<p>
Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige Korrekturen vor:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction avahi "avahi-daemon-check-dns-Mechanismus entfernt, weil er nicht mehr benötigt wird">
<correction base-files "/etc/debian_version auf die 10.9er Zwischenveröffentlichung aktualisiert">
<correction cloud-init "Die erzeugten Passwörter möglichst nicht mehr in allgemein zugängliche Protokolldateien schreiben [CVE-2021-3429]">
<correction debian-archive-keyring "Bullseye-Schlüssel hinzugefügt; Jessie-Schlüssel entfernt">
<correction debian-installer "Linux-Kernel-ABI 4.19.0-16 verwenden">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction exim4 "Verwendung von gleichzeitigen Verbindungen unter GnuTLS überarbeitet; Verifizierung von TLS-Zertifikaten mit CNAMEs überarbeitet; README.Debian: die Beschränkungen bzw. den Umfang der Serverzertifikats-Verifizierung in der Standardkonfiguration dokumentiert">
<correction fetchmail "<q>System error during SSL_connect(): Success</q> (Systemfehler während SSL-connect(): Erfolg) nicht mehr ausgeben; OpenSSL-Versionsüberprüfung entfernt">
<correction fwupd "SBAT-Unterstützung hinzugefügt">
<correction fwupd-amd64-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupd-arm64-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupd-armhf-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupd-i386-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupdate "SBAT-Unterstützung hinzugefügt">
<correction fwupdate-amd64-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupdate-arm64-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupdate-armhf-signed "SBAT-Unterstützung hinzugefügt">
<correction fwupdate-i386-signed "SBAT-Unterstützung hinzugefügt">
<correction gdnsd "Stapelüberlauf bei überlangen IPv6-Adressen behoben [CVE-2019-13952]">
<correction groff "Neukompilierung gegen ghostscript 9.27">
<correction hwloc-contrib "Unterstützung für die ppc64el-Architektur freischalten">
<correction intel-microcode "Verschiedene Microcodes aktualisiert">
<correction iputils "Rundungsfehler bei ping behoben; tracepath-Zielkorrumpierung behoben">
<correction jquery "Anfälligkeit für die Ausführung von vertrauensunwürdigem Code behoben [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Lesezugriff außerhalb der Grenzen (out-of-bounds read) behoben [CVE-2019-20367]">
<correction libpano13 "Anfälligkeit in Format-Zeichenkette behoben">
<correction libreoffice "encodings.py nicht aus dem derzeitigen Verzeichnis laden">
<correction linux "Neue Version der Originalautoren; ABI auf -16 aktualisiert; Secure-Boot-Signierschlüssel rotiert; rt: auf 4.19.173-rt72 aktualisiert">
<correction linux-latest "Update to -15 kernel ABI; update for -16 kernel ABI">
<correction linux-signed-amd64 "Neue Version der Originalautoren; ABI auf -16 aktualisiert; Secure-Boot-Signierschlüssel rotiert; rt: auf 4.19.173-rt72 aktualisiert">
<correction linux-signed-arm64 "Neue Version der Originalautoren; ABI auf -16 aktualisiert; Secure-Boot-Signierschlüssel rotiert; rt: auf 4.19.173-rt72 aktualisiert">
<correction linux-signed-i386 "Neue Version der Originalautoren; ABI auf -16 aktualisiert; Secure-Boot-Signierschlüssel rotiert; rt: auf 4.19.173-rt72 aktualisiert">
<correction lirc "Eingebetteten ${DEB_HOST_MULTIARCH}-Wert in /etc/lirc/lirc_options.conf normalisieren, um unmodifizierte Konfigurationsdateien auf allen Architekturen aufzuspüren; gir1.2-vte-2.91 statt des nicht-existenten gir1.2-vte empfehlen">
<correction m2crypto "Testfehlschlag mit aktuellen OpenSSL-Versionen behoben">
<correction openafs "Probleme mit ausgehenden Verbindungen nach der Unix-Epoche 0x60000000 (14. Januar 2021) behoben">
<correction portaudio19 "Umgang mit EPIPE aus alsa_snd_pcm_poll_descriptors nachgebessert, um einen Absturz zu beheben">
<correction postgresql-11 "Neue Version der Originalautoren; Informationsleck in den constraint-violation-Fehlermeldungen behoben [CVE-2021-3393]; CREATE INDEX CONCURRENTLY nachgebessert, um gleichzeitige vorbereitete Transaktionen abzuwarten">
<correction privoxy "Sicherheitsprobleme [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "CRLF-Injektion in http.client behoben [CVE-2020-26116]; Pufferüberlauf in PyCArg_repr in _ctypes/callproc.c behoben [CVE-2021-3177]">
<correction redis "Serie von Ganzzahlüberläufen auf 32-Bit-Systemen behoben [CVE-2021-21309]">
<correction ruby-mechanize "Probleme mit Befehlsinjektion behoben [CVE-2021-21289]">
<correction systemd "core: sicherstellen, dass auch die Control-Command-ID wiederhergestellt wird, um einen Speicherzugriffsfehler zu lösen; seccomp: Abschalten der Seccomp-Filterung via Umgebungsvariable erlauben">
<correction uim "libuim-data: symlink_to_dir-Umwandlung (von symbolischer Verknüpfung in Verzeichnis) von /usr/share/doc/libuim-data des wiedereingesetzten Pakets vornehmen, um saubere Upgrades von Stretch zu ermöglichen">
<correction xcftools "Anfälligkeit für Ganzzahlüberläufe behoben [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Obergrenze für Auswahlpuffer angepasst, um kombinierende Zeichen unterzubringen [CVE-2021-27135]">
</table>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden Sicherheitsaktualisierungen
hinzu. Das Sicherheitsteam hat bereits für jede davon eine Sicherheitsankündigung
veröffentlicht.</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>


<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Sicherheitskorrekturen enthält,
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Veröffentlichung:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>



