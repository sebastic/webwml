#use wml::debian::ddp title="Debian Benutzer-Handbücher"
#use wml::debian::translation-check translation="3c8a13b77e9d8711624bf06e84d584eb25b58276"
# $Id$
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/user-manuals.defs"
# Translator: Martin Schulze <joey@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2014 - 2015, 2018.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<document "Debian GNU/Linux FAQ" "faq">

<div class="centerblock">
<p>
  Häufig gestellte Fragen und Antworten.

<doctable>
  <authors "Susan G. Kleinmann, Sven Rudolph, Santiago Vila, Josip Rodin, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  fertig
  </status>
  <availability>
  <inpackage "debian-faq"><br>
  <inddpvcs-debian-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Debian-Installationsanleitung" "install">

<div class="centerblock">
<p>
  Installationsanweisungen für die Installation von Debian GNU/Linux.
  Das Handbuch beschreibt den Installationsprozess unter Verwendung des
  Debian-Installers, des Installationssystems, das mit
  <a href="$(HOME)/releases/sarge/">Sarge</a> (Debian GNU/Linux 3.1)
  erstmalig veröffentlicht wurde.<br />
  Zusätzliche Informationen in Hinblick auf den Installer finden Sie in der <a
  href="https://wiki.debian.org/DebianInstaller/FAQ">Debian Installer-FAQ</a> und
  den <a href="https://wiki.debian.org/DebianInstaller">Debian Installer 
  Wiki-Seiten</a>.

<doctable>
  <authors "Debian-Installer-Team">

  <maintainer "Debian-Installer-Team">
  <status>
  Das Handbuch ist noch nicht perfekt. Aktive Arbeit wird geleistet für die
  aktuelle und zukünftige Debian-Veröffentlichungen. Hilfe ist
  willkommen, speziell bei nicht-x86-Installationen und Übersetzungen.
  Kontaktieren Sie
  <a href="mailto:debian-boot@lists.debian.org?subject=Install%20Manual">debian-boot@lists.debian.org</a>
  hinsichtlich weiterer Informationen.
  </status>
  <availability>
  <insrcpackage "installation-guide">
  <br /><br />
  <a href="$(HOME)/releases/stable/installmanual">Veröffentlichte Version für
  das Stable-Release</a>
  <br />
  Auch verfügbar auf den <a href="$(HOME)/CD">offiziellen vollständigen CDs und DVDs</a>
  im Verzeichnis <tt>/doc/manual/</tt>.
  <br /><br />
  <a href="$(HOME)/releases/testing/installmanual">Version, die für das nächste Stable
  vorbereitet wird (in Testing)</a>
  <br /><br />
  <a href="https://d-i.debian.org/manual/">Entwicklungsversion</a>
  <br>
  <inddpvcs-installation-guide>
  </availability>
</doctable>
  Versionen der Installationsanleitung für vorhergehende Veröffentlichungen
  (und möglicherweise die nächste Veröffentlichung) von Debian sind auf der <a 
  href="$(HOME)/releases/">Veröffentlichungsseite</a> für diese 
  Veröffentlichungen verlinkt.
</div>

<hr />

<document "Debian-Veröffentlichungshinweise (Release Notes)" "relnotes">

<div class="centerblock">
<p>
  Dieses Dokument enthält Informationen, was es in der aktuellen
  Distribution Debian GNU/Linux Neues gibt, sowie Hinweise für die
  Aktualisierung älterer Versionen.

<doctable>
  <authors "Adam Di Carlo, Bob Hilliard, Josip Rodin, Anne Bezemer, Rob
  Bradford, Frans Pop, Andreas Barth, Javier Fernández-Sanguino Peña, Steve
  Langasek">
  <status>
  Aktive Arbeit wird bei Debian-Veröffentlichungen geleistet.
  Kontaktieren Sie
  <a href="mailto:debian-doc@lists.debian.org?subject=Release%20Notes">debian-doc@lists.debian.org</a>, um weitere
  Informationen zu erhalten. Probleme und Patches sollten als
  <a href="https://bugs.debian.org/release-notes">Fehler gegen das
  Pseudopaket release-notes</a> berichtet werden.
  </status>
  <availability>
  <a href="$(HOME)/releases/stable/releasenotes">Veröffentlichte Version</a>
  <br />
  Verfügbar auf den <a href="$(HOME)/CD">offiziellen vollen CDs und DVDs</a>
  im Verzeichnis <tt>/doc/release-notes/</tt>.
#  <br />
#  Ebenfalls verfügbar auf
#  <a href="$(HOME)/mirror/list">ftp.debian.org und allen Spiegeln</a>
#  im <tt>/debian/doc/release-notes/</tt>-Verzeichnis.
#  <br />
#  <a href="$(HOME)/releases/testing/releasenotes">Vorbereitete Version für
#  die nächste Stable (in Testing)</a>

  <inddpvcs-release-notes>
  </availability>
</doctable>
</div>

<hr />

<document "Debian-Referenzkarte" "refcard">

<div class="centerblock">
<p>
  Diese Karte stellt neuen Benutzern von Debian GNU/Linux die wichtigsten
  Befehle auf einer einzigen Seite als Referenz für die Arbeit mit Debian
  GNU/Linux-Systemen zur Verfügung. Grundlegende (oder bessere) Kenntnisse
  über Computer, Dateien, Verzeichnisse und die Befehlszeile sind erforderlich.

<doctable>
  <authors "W. Martin Borgert">
  <maintainer "W. Martin Borgert">
  <status>
  veröffentlicht; in aktiver Entwicklung
  </status>
  <availability>
  <inpackage "debian-refcard"><br>
  <inddpvcs-refcard>
  </availability>
</doctable>
</div>

<hr />

<document "Das Debian Administrationshandbuch" "debian-handbook">

<div class="centerblock">
<p>
    Das Debian Administrationshandbuch versorgt jeden, der ein
    effektiver und unabhängiger Debian GNU/Linux-Administrator
    werden möchten, mit dem dafür nötigen Wissen.

<doctable>
  <authors "Raphaël Hertzog, Roland Mas">
  <status>
  veröffentlicht; in aktiver Entwicklung
  </status>
  <availability>
  <inpackage "debian-handbook"><br>
  <inddpvcs-debian-handbook>
  </availability>
</doctable>
</div>

<hr />

<document "Debian-Referenz" "quick-reference">

<div class="centerblock">
<p>
   Diese Debian-GNU/Linux-Referenz behandelt viele Aspekte der
   System-Administration über shell-Befehl-Beispiele. Grundlegende
   Übungen, Tipps und andere Informationen zu Themen wie System-Installation,
   Debian-Paket-Verwaltung, der Linux-Kernel unter Debian, Systemoptimierung,
   Erstellung eines Gateways, Text-Editoren, VCS, Programmierung und GnuPG sind
   verfügbar.

   <p>Vormals <q>Kurzreferenz</q> genannt.

<doctable>
  <authors "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  veröffentlicht; in aktiver Entwicklung
  </status>
  <availability>
  <inpackage "debian-reference"><br>
  <inddpvcs-debian-reference>
  </availability>
</doctable>
</div>

<hr />

<document "Securing-Debian-Handbuch" "securing">

<div class="centerblock">
<p>
  Dieses Handbuch beschreibt die Sicherheit des Debian GNU/Linux
  Betriebssystems und die Sicherheit innerhalb des Debian-Projekts.
  Es beginnt mit dem Prozess des Sicherns und Härtens einer
  standardmäßigen Debian GNU/Linux-Installation (sowohl manuell
  als auch automatisch), deckt einige der beim Einrichten von
  sicheren Benutzer- und Netzwerkumgebungen auftretenden Aufgaben
  ab, liefert Informationen zu verfügbaren Sicherheitswerkzeugen,
  den Schritten vor und nach einem Einbruch und beschreibt außerdem,
  wie Sicherheit in Debian vom Sicherheitsteam Geltung verschafft
  wird. Das Dokument enthält einen Schritt-für-Schritt-Härtungsleitfaden,
  und im Anhang finden sich detaillierte Informationen zum Einrichten
  eines Einbruchserkennungssystems und einer Bridge-Firewall mit
  Debian GNU/Linux.

<doctable>
  <authors "Alexander Reelsen, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  veröffentlicht; in Entwicklung mit nur noch wenigen Änderungen;
  manche Inhalte könnten nicht mehr ganz aktuell sein
  </status>
  <availability>
  <inpackage "harden-doc"><br>
  <inddpvcs-securing-debian-manual>
  </availability>
</doctable>
</div>

<hr />

<document "aptitude-Benutzeranleitung" "aptitude">

<div class="centerblock">
<p>
    Eine einführende Anleitung in die Verwendung des aptitude-Paketmanagers,
    inklusive der vollständigen Befehlsreferenz.

<doctable>
  <authors "Daniel Burrows">
  <status>
  veröffentlicht; in aktiver Entwicklung
  </status>
  <availability>
  <inpackage "aptitude-doc"><br>
  <inddpvcs-aptitude>
  </availability>
</doctable>
</div>

<hr />

<document "APT-Benutzeranleitung" "apt-guide">

<div class="centerblock">
<p>
    Dieses Dokument gibt einen Überblick über das
    APT-Paketmanagement-Programm.

<doctable>
  <authors "Jason Gunthorpe">
  <status>
  veröffentlicht; ein bisschen veraltet
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-guide>
  </availability>
</doctable>
</div>

<hr />

<document "APT Offline verwenden" "apt-offline">

<div class="centerblock">
<p>
    Dieses Dokument beschreibt, wie Sie APT in einer Umgebung ohne
    Netzwerkanbindung verwenden, speziell ein Ansatz namens <q>sneaker-net</q>
    zur Durchführung von System-Upgrades.

<doctable>
  <authors "Jason Gunthorpe">
  <status>
  veröffentlicht; ein bisschen veraltet
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-offline>
  </availability>
</doctable>
</div>

<hr />

<document "Die Debian Java-FAQ" "java-faq">

<div class="centerblock">
<p>
  Der Sinn dieser FAQ besteht darin, einen Platz zu bieten, wo alle
  Arten von Fragen zu Java und Debian beantwortet werden. Es enthält
  Lizenzfragen, verfügbare Entwicklungspakete und Programme, die zur
  Erstellung einer Umgebung für Freies-Software-Java nötig sind.

<doctable>
  <authors "Javier Fernández-Sanguino Peña, Torsten Werner, Niels Thykier, Sylvestre Ledru">
  <status>
  veröffentlicht; in aktiver Entwicklung, allerdings könnten Teile des 
  Inhalts eventuell veraltet sein
  </status>
  <availability>
  <inpackage "java-common"><br>
  <inddpvcs-debian-java-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Debian-Handbuch für Hamradio-Paketbetreuer" "hamradio-maintguide">

<div class="centerblock">
<p>
Dieses Debian-Handbuch behandelt Teamrichtlinien und derzeitige Best Practises
für das Packaging-Team von Debian Hamradio.

<doctable>
  <authors "Iain R. Learmonth">
  <status>
  veröffentlicht; in aktiver Entwicklung
  </status>
  <availability>
  <inpackage "hamradio-maintguide"><br>
  <inddpvcs-hamradio-maintguide>
  </availability>
</doctable>
</div>
